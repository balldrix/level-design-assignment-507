<?xml version="1.0" encoding="UTF-8"?>
<tileset name="maze" tilewidth="512" tileheight="512" tilecount="4" columns="0">
 <terraintypes>
  <terrain name="New Terrain" tile="-1"/>
 </terraintypes>
 <tile id="2" terrain="0,0,0,">
  <image width="512" height="512" source="../Assets/Textures/path.png"/>
 </tile>
 <tile id="3">
  <image width="512" height="512" source="../Assets/Textures/wood.png"/>
 </tile>
 <tile id="7">
  <image width="512" height="512" source="../Assets/Textures/woodenexit.png"/>
 </tile>
 <tile id="8">
  <image width="512" height="512" source="../Assets/Textures/woodenentrance.png"/>
 </tile>
</tileset>
