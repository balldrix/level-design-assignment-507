<?xml version="1.0" encoding="UTF-8"?>
<tileset name="NPC" tilewidth="512" tileheight="512" tilecount="1" columns="0">
 <terraintypes>
  <terrain name="New Terrain" tile="-1"/>
 </terraintypes>
 <tile id="2">
  <image width="512" height="512" source="../Assets/Icons/NPCIcon.png"/>
 </tile>
</tileset>
