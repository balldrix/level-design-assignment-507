<?xml version="1.0" encoding="UTF-8"?>
<tileset name="items" tilewidth="512" tileheight="512" tilecount="5" columns="0">
 <tile id="0">
  <image width="512" height="512" source="../Assets/Icons/greenflag512.png"/>
 </tile>
 <tile id="1">
  <image width="512" height="512" source="../Assets/Icons/redflag512.png"/>
 </tile>
 <tile id="2">
  <image width="512" height="512" source="../Assets/Icons/ChargeUpIcon.png"/>
 </tile>
 <tile id="3">
  <image width="512" height="512" source="../Assets/Icons/DoubleDamageIcon.png"/>
 </tile>
 <tile id="4">
  <image width="512" height="512" source="../Assets/Icons/RapidFireIcon.png"/>
 </tile>
</tileset>
