// Graphics.h
// Christopher Ball 2016
// This class manages DirectX and all the functions for 
// drawing to the screen

#ifndef _GRAPHICS_H_
#define _GRAPHICS_H_

#include "pch.h"

class Graphics
{
public:
	Graphics();					
	~Graphics();			
	void						Init(HWND hWindow, HINSTANCE hInstance);	// initialises graphics device
	void						ReleaseAll();								// release all ID3D11 pointers
	void						SetWorldMatrix(Matrix world);				// use world, view and projection matrices to send final to constant buffer
	void						SetViewMatrix(Matrix view);					// pass view matrix from camera
	void						SetProjectionMatrix(Matrix projection);		// pass projection matrix from camera
	void						SetUVTransform(Matrix transform);			// pass uv coord transform matrix

	void						BeginScene();								// prepare render target for rendering scene
	
	void						PresentBackBuffer();						// present backbuffer to screen

	ID3D11Device*				GetDevice() { return m_D3DDevice; }			// return graphics device pointer
	ID3D11DeviceContext*		GetDeviceContext() { return m_D3DDeviceContext; }	// return graphics context pointer
	
	float GetWidth()	const { return m_gameWidth; }					// return game window width
	float GetHeight()	const { return m_gameHeight; }					// return game window height
	
	HWND GetHwnd()		const { return m_hWnd; }						// return window handle

private:
	HWND						m_hWnd;					// handle to the window
	HINSTANCE					m_hInstance;			// instance of our window
	
	IDXGISwapChain*				m_swapchain;			// pointer to swapchain of frame buffers
	ID3D11Device*				m_D3DDevice;			// pointer to D3D Device
	ID3D11DeviceContext*		m_D3DDeviceContext;		// pointer to D3D Context

	ID3D11RenderTargetView*		m_renderTargetView;		// pointer to render target view
	ID3D11Texture2D*			m_backbuffer;			// pointer to 2d backbuffer
	
	ID3D11DepthStencilView*		m_depthBuffer;			// pointer to depth view resource	
	ID3D11Texture2D*			m_depthBufferTexture;	// pointer to depth buffer

	ID3D11BlendState*			m_blendState;			// blend state for alpha blending

	ID3D11Buffer*				m_constantBuffer;		// pointer to the constant buffer for shaders
	ID3D11Buffer*				m_textureTranslation;	// constant buffer for texture animation
	ID3D11SamplerState*			m_linearSampler;		// pointer to samplerstate for texture sampling
	
	Matrix						m_viewMatrix;			// view matrix
	Matrix						m_projectionMatrix;		// projection matrix

	bool						m_fullScreen;			// fullscreen setting
	
	float						m_gameWidth;			// client window width
	float						m_gameHeight;			// client window height
};

#endif _GRAPHICS_H_
