#include "Pickup.h"

Pickup::Pickup() :
	m_scale(0.0f)
{
}

Pickup::~Pickup()
{}

void
Pickup::Update()
{
	// scale and translate pickup
	m_scale = 0.5f;
	m_position.y = -0.5f;

	m_scaleMatrix = XMMatrixScaling(m_scale, m_scale, m_scale);
	m_translationMatrix = XMMatrixTranslationFromVector(m_position);
	m_modelMatrix = m_scaleMatrix * m_rotationalMatrix * m_translationMatrix;
}
