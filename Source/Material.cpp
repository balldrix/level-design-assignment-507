#include "Material.h"
#include "Graphics.h"
#include "Texture.h"
#include "Shader.h"

Material::Material()
{
}

Material::~Material()
{
}

void 
Material::Init(Texture* texture, Shader* shader)
{
	m_texture = texture->GetTexture();
	m_vertexShader = shader->GetVertexShader();
	m_pixelShader = shader->GetPixelShader();
	m_inputLayout = shader->GetInputLayout();
}

void 
Material::RenderSetup(Graphics* graphics)
{
	// set shader resource *texture*
	graphics->GetDeviceContext()->PSSetShaderResources(0, 1, &m_texture);

	// set vertex shader
	graphics->GetDeviceContext()->VSSetShader(m_vertexShader, nullptr, 0);

	// set pixel shader
	graphics->GetDeviceContext()->PSSetShader(m_pixelShader, nullptr, 0);

	// set input layout
	graphics->GetDeviceContext()->IASetInputLayout(m_inputLayout);

}
