// ShadowMesh.h
// Christopher Ball 2016
// mesh for blob shadow

#ifndef _SHADOWMESH_H_
#define _SHADOWMESH_H_

#include "MeshObject.h"

// forward declarations
class Graphics;

// inherit from MeshObject
class ShadowMesh : public MeshObject
{
public:
	ShadowMesh();
	~ShadowMesh();
	void Init(Graphics* graphics); // initialise shadow mesh
};

#endif _SHADOWMESH_H_