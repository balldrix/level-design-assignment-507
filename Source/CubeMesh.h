// CubeMesh.h
// Christopher Ball 2016
// child of MeshObject, holds data for blocks
// that make up the maze walls

#ifndef _CubeMesh_H_
#define _CubeMesh_H_

#include "MeshObject.h"

// forward declarations
class Graphics;

// inherits MeshObject class
class CubeMesh : public MeshObject
{
public:
	CubeMesh();
	~CubeMesh();
	void Init(Graphics* graphics); // initialise CubeMesh
};

#endif _CubeMesh_H_
