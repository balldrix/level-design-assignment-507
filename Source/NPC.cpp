#include "NPC.h"
#include "pch.h"
#include "Shadow.h"
#include "Constants.h"
#include "Randomiser.h"
#include "Utility.h"
#include "UnitVectors.h"

NPC::NPC() :
	m_NPCSound(nullptr),
	m_movementSFX(nullptr),
	m_hitSFX(nullptr),
	m_deadSFX(nullptr),
	m_target(nullptr),
	m_direction(Vector3(0.0f, 0.0f, 0.0f)),
	m_tracking(Vector3(0.0f, 0.0f, 0.0f)),
	m_speed(0.0f),
	m_hp(0),
	m_NPCState(NPC_ROAMING),
	m_NPCAnimState(NPC_ANIM_ALIVE),
	m_spriteData({0}),
	m_movementCounter(Vector2(0.0f, 0.0f)),
	m_alerted(false),
	m_lineOfSight(false)
{
	m_animation.done = false;
	m_animation.timer = NPC__SHOT_ANIM_TIME;
}

NPC::~NPC()
{
}

void
NPC::Update(float deltaTime)
{
	// check npc ai state
	switch(m_NPCState)
	{
		case NPC_ROAMING:
			m_NPCState = Roaming();
			break;
		case NPC_HOMING:
			m_NPCState = Homing();
			break;
		case NPC_TRACKING:
			m_NPCState = Tracking();
			break;
		case NPC_SHOT:
			m_NPCState = Shot();
			break;
		case NPC_DEAD:
			m_NPCState = Dead();
			break;
		default:
			break;
	}

	// set velocity with movement direction x speed
	m_velocity = (m_direction * m_speed) * deltaTime;

	// update position with new velocity
	m_position = m_position + m_velocity;

	// set aabb
	m_hitbox.SetAABB(Vector3(-1.0f, -1.0f, 0.0f), Vector3(1.0f, 1.0f, 0.0f));
	m_hitbox.OffSetAABB(m_position);

	// get length of distance from npc to player
	float distance = (m_position - m_target->GetPosition()).Length();

	// if distance is within 8 squares
	if(distance < 8.0f * (float)GlobalConstants::CELL_WIDTH)
	{
		// if movement counter crosses a square
		if(m_movementCounter.x > GlobalConstants::CELL_WIDTH ||
		   m_movementCounter.y > GlobalConstants::CELL_WIDTH)
		{
			// reset movement counter
			m_movementCounter = Vector2(0.0f, 0.0f);

			// if the npc has a valid movement sfx
			if(m_movementSFX)
			{
				// set positional audio listener and emitters
				AudioListener listener;
				listener.SetPosition(m_target->GetPosition()); // player position
				listener.SetOrientation(m_target->GetFacing(), Vector3(0.0f, 1.0f, 0.0f)); // player facing direction
				AudioEmitter emitter;
				emitter.SetPosition(m_position); // npc position
				emitter.SetOrientation(m_direction, Vector3(0.0f, 1.0f, 0.0f)); // npc movement direction

				// create sfx instance
				m_NPCSound = m_movementSFX->CreateInstance(SoundEffectInstance_Use3D |																			 SoundEffectInstance_ReverbUseFilters);

				// play sound
				m_NPCSound->Play();

				// apply positional 3D
				m_NPCSound->Apply3D(listener, emitter, false);
			}
		}
	}

	// increase movement counter to 
	// check when to play sound
	m_movementCounter.x += m_velocity.x;
	m_movementCounter.y += abs(m_velocity.z);

	if(m_target) // check npc has a target turn to face target
	{
		// get vector point of target
		Vector3 lookAt = m_target->GetPosition() - m_position;
		lookAt.Normalize();

		// angle is inverse cos of look at z axis
		float angle = acos(lookAt.z);

		// if player left of enemy
		if(lookAt.x < 0)
		{
			// inverse angle
			angle *= -1.0f;
		}

		// set rotation matrix by rotating around y
		m_rotationalMatrix = XMMatrixRotationY(angle);
	}
	
	// translate model position
	m_modelMatrix = XMMatrixTranslationFromVector(m_position);

	// tranform block matrix
	m_modelMatrix = m_rotationalMatrix * m_modelMatrix;

	// animate
	m_spriteData.frameWidth = 512;
	m_spriteData.frameHeight = 512;
	m_spriteData.sheetWidth = 2048;
	m_spriteData.sheetHeight = 2048;
	m_spriteData.columns = m_spriteData.sheetWidth / m_spriteData.frameWidth;
	m_animation.currentFrame = 0;

	// set UV scale and offset
	float UVScale = 1.0f / m_spriteData.columns;
	float UOffset = UVScale * m_animation.currentFrame;
	float VOffset = UVScale * m_NPCAnimState;

	// transform texture uv
	m_animMatrix._11 = UVScale;
	m_animMatrix._22 = UVScale;
	m_animMatrix._31 = UOffset;
	m_animMatrix._32 = VOffset;

	// if shot animation is true
	if(m_NPCAnimState == NPC_ANIM_SHOT)
	{
		// time animation 
		m_animation.timer -= deltaTime;
	}

	// if animation is over
	if(m_animation.timer < 0.0f)
	{
		// reset timer
		m_animation.timer = NPC__SHOT_ANIM_TIME;
		
		// set animation to done
		m_animation.done = true;
		m_NPCAnimState = NPC_ANIM_ALIVE;
	}
}

void NPC::Release()
{
	m_NPCSound.reset();
}

void 
NPC::SetGoalVelocity(const Vector3& targetVel)
{
}

void
NPC::SetTarget(GameObject* target)
{
	m_target = target;
}

void NPC::SetDirection(const Vector3 & direction)
{
	m_direction = direction;
}

void 
NPC::SetSpeed(const int& speed)
{
	m_speed = speed;
}

void 
NPC::SetHP(const int& hp)
{
	m_hp = hp;
}

void 
NPC::SetAIState(const NPCAIState& state)
{
	m_NPCState = state;
}

void 
NPC::SetAnimState(const NPCAnimState& state)
{
	m_NPCAnimState = state;
}

void 
NPC::SetAlerted(bool alerted)
{
	m_alerted = alerted;
}

void 
NPC::SetLineOfSight(bool los)
{
	m_lineOfSight = los;
}

void 
NPC::SetRandomDirection()
{
	// randomise direction of movement
	int direction = Randomiser::GetRandNum(0, 3);
	switch(direction)
	{
		case 0:
			SetDirection(MapDirections::North); // north
			break;
		case 1:
			SetDirection(MapDirections::East); // east
			break;
		case 2:
			SetDirection(MapDirections::South); // south
			break;
		case 3:
			SetDirection(MapDirections::West); // west
			break;
		default:
			SetDirection(Vector3(0.0f, 0.0f, 0.0f)); /// none
			break;
	}
}

void 
NPC::SetMovementSFX(SoundEffect* sound)
{
	m_movementSFX = sound;
}

void 
NPC::SetHitSFX(SoundEffect* sound)
{
	m_hitSFX = sound;
}

void 
NPC::SetDeadSFX(SoundEffect* sound)
{
	m_deadSFX = sound;
}

NPCAIState 
NPC::Roaming()
{	
	if(m_lineOfSight)
	{	
		return NPC_HOMING;
	}
	else // if player is hidden then
	{
		return NPC_ROAMING; // set NPC to roaming state

		if(m_alerted) // if npc has been previously alerted then 
		{
			return NPC_TRACKING; // set NPC state to tracking
		}
	}
}

NPCAIState 
NPC::Homing()
{
	// if NPC has line of sight
	if(m_lineOfSight)
	{
		m_alerted = true; // NPC has been alerted to player position
		m_tracking = m_target->GetPosition(); // set tracking position to player position
		m_direction = m_tracking - m_position; // move direction is towareds the player 
		m_direction.Normalize(); // get normal vector
		return NPC_HOMING; // return homing state
	}

	// if NPC alerted to target position
	if(m_alerted)
	{
		return NPC_TRACKING; // return tracking state
	}
	else
	{
		return NPC_ROAMING; // return roaming state
	}
}

NPCAIState 
NPC::Tracking()
{
	// if target is in line of sight
	if(m_lineOfSight)
	{
		return NPC_HOMING; // return homing state
	}
	else
	{
		return NPC_TRACKING; // return tracking state
	}
}

NPCAIState
NPC::Shot()
{
	// if the npc has a valid hit sfx
	if(m_hitSFX)
	{
		// set positional audio listener and emitters
		AudioListener listener;
		listener.SetPosition(m_target->GetPosition()); // player position
		listener.SetOrientation(m_target->GetFacing(), Vector3(0.0f, 1.0f, 0.0f)); // player facing direction
		AudioEmitter emitter;
		emitter.SetPosition(m_position); // npc position
		emitter.SetOrientation(m_direction, Vector3(0.0f, 1.0f, 0.0f)); // npc movement direction

		// create sfx instance
		m_NPCSound = m_hitSFX->CreateInstance(SoundEffectInstance_Use3D | SoundEffectInstance_ReverbUseFilters);

		// play sound
		m_NPCSound->Play();

		// apply positional 3D
		m_NPCSound->Apply3D(listener, emitter, false);
	}

	// if animation is done
	if(m_animation.done)
	{
		// check if npc is dead
		if(m_hp < 1)
		{
			// if the npc has a valid hit sfx
			if(m_deadSFX)
			{
				// set positional audio listener and emitters
				AudioListener listener;
				listener.SetPosition(m_target->GetPosition()); // player position
				listener.SetOrientation(m_target->GetFacing(), Vector3(0.0f, 1.0f, 0.0f)); // player facing direction
				AudioEmitter emitter;
				emitter.SetPosition(m_position); // npc position
				emitter.SetOrientation(m_direction, Vector3(0.0f, 1.0f, 0.0f)); // npc movement direction

				// create sfx instance
				m_NPCSound = m_deadSFX->CreateInstance(SoundEffectInstance_Use3D | SoundEffectInstance_ReverbUseFilters);

				// play sound
				m_NPCSound->Play();

				// apply positional 3D
				m_NPCSound->Apply3D(listener, emitter, false);
			}
			// hp is up return dead state
			return NPC_DEAD;
		}
		// npc is not dead set alive animation
		m_NPCAnimState = NPC_ANIM_ALIVE;
		return NPC_ROAMING; // return roaming state
	}
	return NPC_SHOT; // return shot state
}

NPCAIState 
NPC::Dead()
{
	m_NPCAnimState = NPC_ANIM_DEAD; // set anim state to dead
	m_direction = Vector3(0.0f, 0.0f, 0.0f); // set direction 0
	return NPC_DEAD;
}