// Cube.h
// Christopher Ball 2016
// this class manages the cube

#ifndef _CUBE_H_
#define _CUBE_H_

#include "pch.h"
#include "IRenderable.h"
#include "GameObject.h"
#include "Graphics.h"
#include "AABB.h"

class Cube : public IRenderable, GameObject
{
public:
	Cube(); // default constructor
	~Cube(); // default destructor
	GameObject& Instance() { return *this; } // returns instance of game object
	void Init(Graphics* graphics); // initialises cube vertex and index buffers
	void Render(Graphics* graphics); // renders cube
	void SetPosition(Vector3 position); // set world position of cube
	void ReleaseAll();	// release pointers
	Vector3 GetPosition() const { return m_position; } // get cube position	
private:
	ID3D11Buffer*				m_pVertexBuffer;		// pointer to vertex buffer
	ID3D11Buffer*				m_pIndexBuffer;			// pointer to index buffer
	ID3D11ShaderResourceView*	m_pTexture; // pointer to texture resource
	
	int                         m_vertexCount; // mesh data
	int                         m_indexCount; // mesh data

	Vector3						m_position; // x, y, z position of cube
};

#endif _CUBE_H_
