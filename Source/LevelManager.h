// LevelManager.h
// Christopher Ball 2016
// This class manages the level by storing 
// container of levels to load

#ifndef _LEVEL_MANAGER_H
#define _LEVEL_MANAGER_H

#include "Level.h"

// enum of list of levels
enum LevelList
{
	LEVEL_ONE = 1, // start enum at 1
	LEVEL_TWO,
	LEVEL_THREE,
	LEVEL_FOUR,
	LEVEL_FIVE,
	LEVEL_SIX,
	LEVEL_SEVEN,
	LEVEL_EIGHT,
	LEVEL_NINE,
	LEVEL_TEN,
	LEVEL_ELEVEN,
	LEVEL_TWELVE,
	MAX_LEVELS // used to check if reached last level
};

class LevelManager
{
public:
	LevelManager();
	~LevelManager();

	bool		 Init(); // initialise level manager
	bool		 SwitchLevel(); // switch level to next one
	bool		 LoadLevel(unsigned int num); // load specific level
	void		 SetLevel(int num); // set current level
	Level*		 GetLevel() const { return m_level; } // get current level number
	unsigned int GetCurrentLevel() const { return m_currentLevel; } // get pointer to current loaded level
	void		 DeleteLevel(); // delete level to clear maps

private:
	unsigned int m_currentLevel; // current level number 
	Level*		 m_level; // pointer to level class
	LevelList	 m_levelList; // list of level enumerator
};

#endif _LEVEL_MANAGER_H