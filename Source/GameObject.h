// GameObject.h
// Christopher Ball 2016
// A base class for renderable objects
// Modified code from SumoDX by Mickey Macdonald @scruffyfurn

#ifndef _GAMEOBJECT_H_
#define _GAMEOBJECT_H_

#include "pch.h"
#include "AABB.h"

// forward definitions
class MeshObject;
class Material;
class Graphics;
class Shadow;

class GameObject
{
public:
	GameObject();
	~GameObject();
	
	void			Init(MeshObject* mesh, 
						 Material* material, 
						 Vector3 position = Vector3(0.0f, 0.0f, 0.0f)); // initialise object

	virtual void	Update() {};					// update object
	void			Render(Graphics* graphics);		// render object

	// helper methods
	// setters
	void			SetID(const wchar_t* string);
	void			SetPosition(Vector3 position);
	void			SetVelocity(Vector3 velocity);
	void			SetShadow(Shadow* shadow); // add shadow to AI object

	// getters
	const wchar_t*	GetID() const { return m_ID; }
	Vector3			GetPosition() const { return m_position; }
	Vector3			GetVelocity() const { return m_velocity; }
	AABB			GetHitBox()	const	{ return m_hitbox; } // return hitbox
	virtual	Vector3	GetFacing() const { return m_defaultZAxis; }

	void			RenderShadow(Graphics* graphics); // render shadow
	
protected:
	const wchar_t*	m_ID;				// object ID
	MeshObject*		m_mesh;				// model mesh
	Material*		m_material;			// model material

	Matrix			m_modelMatrix;		// model world matrix
	Matrix			m_scaleMatrix;		// scale matrix
	Matrix			m_translationMatrix;// translation matrix
	Matrix			m_rotationalMatrix;	// rotation matrix
	Matrix			m_animMatrix;		// texture transform matrix

	Vector3			m_position;			// object position
	Vector3			m_velocity;			// object movement velocity
	Vector3			m_defaultXAxis;		// default x axis
	Vector3			m_defaultYAxis;		// default y axis
	Vector3			m_defaultZAxis;		// default z axis

	Shadow*			m_shadow;			// shadow blob

	AABB			m_hitbox;			// axis aligned bounding box
};

#endif _GAMEOBJECT_H_