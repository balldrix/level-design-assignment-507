#include "pch.h"
#include "LevelManager.h"
#include "Level.h"

LevelManager::LevelManager() :
	m_currentLevel(0),
	m_level(nullptr)
{
}

LevelManager::~LevelManager()
{
	if(m_level) // if level is not null
	{
		// delete level and null pointer
		delete m_level; 
		m_level = nullptr;
	}
}

bool 
LevelManager::Init()
{
	// init level pointer
	m_level = new Level();

	// set level to first level in list
	m_currentLevel = LEVEL_ONE;

	// load level
	if(!LoadLevel(m_currentLevel))
	{
		return false; // if level fails to load return false
	}
	return true; // level loaded ok and wasn't the last level
}

bool
LevelManager::SwitchLevel()
{
	m_currentLevel++; // increase level 

	if(m_currentLevel == MAX_LEVELS)
	{
		return false; // if all levels are complete return false
	}

	if(!LoadLevel(m_currentLevel))
	{
		return false; // if level fails to load return false
	}
	return true; // level loaded ok and wasn't the last level
}

bool 
LevelManager::LoadLevel(unsigned int num)
{
	// set num to string value num
	std::string digit = std::to_string(num); 

	// set file paths with contatenation
	std::string mapFilepath = "Assets\\Levels\\Level" + digit + "\\Level" + digit + "_MazeObjects.csv";
	std::string itemFilepath = "Assets\\Levels\\Level" + digit + "\\Level" + digit + "_Items.csv";
	std::string NPCPositionData = "Assets\\Levels\\Level" + digit + "\\Level" + digit + "_NPCs.csv";
	std::string NPCDifficulty = "Assets\\Levels\\Level" + digit + "\\Level" + digit + "_NPCData.csv";

	// initialise level
	if(!m_level->Init(mapFilepath, itemFilepath, NPCPositionData, NPCDifficulty))
	{
		return false; // if loading fails return false
	}
	return true; // level loading worked
}

void LevelManager::SetLevel(int num)
{
	m_currentLevel = num;
}

void LevelManager::DeleteLevel()
{
	m_level->DeleteMaps(); // delete maps stored in containers
}
