#include "Utility.h"
#include "Constants.h"

Vector3 Utility::GridToWorldSpace(unsigned int x, unsigned int z)
{
	// return world position using 
	// map grid location
	Vector3 position;
	position.x = x * (float)GlobalConstants::CELL_WIDTH;
	position.y = 0;
	position.z = z * (float)GlobalConstants::CELL_WIDTH;
	position.z *= -1; // invert z as the maze is rendered in -z direction

	return position;
}