// AABB.h
// Christopher Ball 2016
// axis aligned bounding box
// for collision detection

#ifndef _AABB_H_
#define _AABB_H_

#include "pch.h"

class AABB
{
public:
	AABB();
	AABB(const Vector3 &min, const Vector3 &max);
	~AABB();
	
	// helper methods
	void				SetAABB(const AABB &hitbox); // set hit box to new aabb points
	void				SetAABB(const Vector3 &min, const Vector3 &max); // set min and max bounds
	void				OffSetAABB(float x, float y, float z); // offset min and max
	void				OffSetAABB(Vector3 offSet); // offset min and max
	void				SetMin(const Vector3 &min); // set min vector
	void				SetMax(const Vector3 &max); // set max vector

	const Vector3&		GetMin() const { return m_min; } // get min vector
	const Vector3&		GetMax() const { return m_max; } // get max vector

	bool				Collision(const AABB &other) const; // return true if collision occurs with other
	virtual void		Reset(); // reset min and max to zero

	AABB&				operator=(const AABB &other); // = operator overload

private:
	Vector3				m_min; // min vector point
	Vector3				m_max; // max vector point
};

#endif _AABB_H_