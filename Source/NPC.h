// NPC.h
// Christopher Ball 2016
// Enemy ai which patrols the maze,
// it has a waypoint container to iterate through

#ifndef _NPC_H_
#define _NPC_H_

#include "pch.h"
#include "GameObject.h"
#include "Animation.h"

// forward declarations
class Player;

// npc states for ai
enum NPCAIState
{
	NPC_ROAMING,
	NPC_HOMING,
	NPC_TRACKING,
	NPC_SHOT,
	NPC_DEAD
};

// npc animation states
enum NPCAnimState
{
	NPC_ANIM_ALIVE,
	NPC_ANIM_SHOT,
	NPC_ANIM_DEAD
};

// max time of npc shot animation
const float NPC__SHOT_ANIM_TIME = 0.05f;

// inherit from game object
class NPC : public GameObject
{
public:
	NPC();
	~NPC();
	void			Update(float deltaTime); // update model matrix
	void			Release(); // release unique pointers

	// helper methods
	// setters
	void			SetGoalVelocity(const Vector3& targetVel); // set target velocity
	void			SetTarget(GameObject* target); // set target vector
	void			SetDirection(const Vector3& direction); // set movement direction
	void			SetSpeed(const int& speed); // set movement speed
	void			SetHP(const int& hp); // set health points
	void			SetAIState(const NPCAIState& state); // set ai state
	void			SetAnimState(const NPCAnimState& state); // set animation state
	void			SetAlerted(bool alerted); // set alerted state
	void			SetLineOfSight(bool los); // set line of sight state
	void			SetRandomDirection(); // randomise movement direction

	// set sound effects
	void			SetMovementSFX(SoundEffect* sound); 
	void			SetHitSFX(SoundEffect* sound);
	void			SetDeadSFX(SoundEffect* sound);

	// getters
	GameObject*		GetTarget() const { return m_target; }
	Vector3			GetDirection() const { return m_direction; }
	float			GetSpeed() const { return m_speed; }
	int				GetHP() const { return m_hp; }
	Vector3			GetTracking() const { return m_tracking; }
	NPCAIState		GetState() const { return m_NPCState; }
	bool			GetAlerted() const { return m_alerted; }
	SoundEffect*	GetMovementSFX() const { return m_movementSFX; }
	SoundEffect*	GetHitSFX() const { return m_hitSFX; }
	SoundEffect*	GetDeadSFX() const { return m_deadSFX; }

	// state methods
	NPCAIState		Roaming();
	NPCAIState		Homing();
	NPCAIState		Tracking();
	NPCAIState		Shot();
	NPCAIState		Dead();

private:
	std::unique_ptr<SoundEffectInstance> m_NPCSound; // NPC sound effect instance
	SoundEffect*	m_movementSFX; // npc movement sound
	SoundEffect*	m_hitSFX; // npc shot sound
	SoundEffect*	m_deadSFX; // npc dead sound

	GameObject*		m_target; // player target
	Vector3			m_direction; // facing direction 
	Vector3			m_tracking; // last seen target postion

	float			m_speed; // movement speed
	int				m_hp; // health points

	Animation		m_animation; // animation timing

	NPCAIState		m_NPCState; // ai state
	NPCAnimState	m_NPCAnimState; // animation state
	
	SpriteSheet		m_spriteData; // sprite sheet data

	Vector2			m_movementCounter; // movement counter

	bool			m_alerted; // has been alerted to player
	bool			m_lineOfSight; // NPC has line of sight of target
};

#endif _NPC_H_
