#include "Level.h"
#include "pch.h"

Level::Level()
{
}

Level::~Level()
{
}

bool
Level::Init(std::string mapFile, std::string itemsFile, std::string NPCPositionFile, std::string NPCDifficulty)
{
	// load maze
	if(!LoadMaze(mapFile)) // load the map data into the array from a file
	{
		return false; // if file loading failed
	}

	// load item file
	if(!LoadItemMap(itemsFile)) // load the item map data into the array from a file
	{
		return false; // if file loading failed
	}

	// load npc position data
	if(!LoadNPCPositions(NPCPositionFile))
	{
		return false; // if file loading failed
	}

	// load npc difficulty data
	if(!LoadNPCData(NPCDifficulty))
	{
		return false; // if file loading failed
	}
	return true;
}

bool
Level::LoadMaze(std::string filename)
{
	char comma = ' '; // char to read commas in csv file
	std::ifstream file; // ifstream file buffer
	file.open(filename); // opens file and reads to buffer
	if(file) // if file is open
	{
		for(unsigned int z = 0; z < MAZE_DEPTH; z++)
		{
			for(unsigned int x = 0; x < MAZE_WIDTH; x++)
			{
				// use temp char so code can skip commas
				int temp = 0;
			
				file >> temp; // int from file into temp var
				if(x != MAZE_WIDTH - 1)
				{
					file >> comma;
				}
			
				m_map.push_back(temp); // set the character to the grid space in map array
			}
		}
	}
	else
	{	
		return false;
	}
	file.close(); // close file
	return true;
}

bool 
Level::LoadNPCPositions(std::string filename)
{
	char comma = ' '; // char to read commas in csv file
	std::ifstream file; // ifstream file buffer
	file.open(filename); // opens file and reads to buffer
	if(file) // if file is open
	{
		 // use temp char so code can skip commas
		int temp = 0;
		for(unsigned int z = 0; z < MAZE_DEPTH; z++)
		{
			for(unsigned int x = 0; x < MAZE_WIDTH; x++)
			{
				file >> temp; // read int from file into temp var

				if(x != MAZE_WIDTH - 1)
				{
					file >> comma;
				}

				m_NPCMap.push_back(temp); // set the character to the grid space in map array
			}
		}
	}
	else
	{	
		return false;
	}
	file.close(); // close file
	return true;	
}

bool 
Level::LoadNPCData(std::string filename)
{
	char comma = ' '; // char to read commas in csv file
	std::ifstream file; // ifstream file buffer
	file.open(filename); // opens file and reads to buffer
	if(file) // if file is open
	{
		// read npc hp
		file >> m_NPCData.hp; // read int from file into npc hp

		file >> comma; // read comma

		// read npc speed
		file >> m_NPCData.speed; // read int from file into npc speed

	}
	else
	{	
		return false;
	}
	file.close(); // close file
	return true;	
}

bool
Level::LoadItemMap(std::string filename)
{
	char comma = ' '; // char to read commas in csv file
	std::ifstream file; // ifstream file buffer
	file.open(filename); // opens file and reads to buffer
	if(file) // if file is open
	{
		// use temp char so code can skip commas
		int temp = 0;
		for(unsigned int z = 0; z < MAZE_DEPTH; z++)
		{
			for(unsigned int x = 0; x < MAZE_WIDTH; x++)
			{
				file >> temp; // read int from file into temp var

				if(x != MAZE_WIDTH - 1)
				{
					file >> comma;
				}

				m_itemMap.push_back(temp); // set the character to the grid space in map array
			}
		}
	}
	else
	{	
		return false;
	}
	file.close(); // close file
	return true;	
}

void
Level::DeleteMaps()
{
	m_map.clear();
	m_itemMap.clear();
	m_NPCMap.clear();
}
