#include "ShadowMesh.h"
#include "Graphics.h"
#include "ConstantBuffers.h"

ShadowMesh::ShadowMesh()
{
}

ShadowMesh::~ShadowMesh()
{
}

void
ShadowMesh::Init(Graphics* graphics)
{
	// setup plain buffers for shadow 
	// texture blob

	D3D11_BUFFER_DESC bd = { 0 };
	D3D11_SUBRESOURCE_DATA subData = { 0 };

	// vertex buffer
	Vertex planeVerts[] = 
	{
		{ Vector3(-1.0f, -0.99f, -1.0f), Vector2(0.0f, 1.0f) }, // bottom left
		{ Vector3( 1.0f, -0.99f, -1.0f), Vector2(1.0f, 1.0f) }, // bottom right
		{ Vector3( 1.0f, -0.99f,  1.0f), Vector2(1.0f, 0.0f) }, // top right
		{ Vector3(-1.0f, -0.99f,  1.0f), Vector2(0.0f, 0.0f) }, // top left
	};

	// index buffer
	WORD planeIndices[] =
	{
		0, 2, 1,
		3, 2, 0
	};

	m_vertexCount = 4;
	m_indexCount = ARRAYSIZE(planeIndices);

	// create vertex buffer for a cube
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(Vertex) * ARRAYSIZE(planeVerts);
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	subData = { planeVerts, 0, 0 };
	graphics->GetDevice()->CreateBuffer(&bd, &subData, &m_vertexBuffer);

	// create index buffer
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(short) * m_indexCount;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	subData = { planeIndices, 0, 0 };
	graphics->GetDevice()->CreateBuffer(&bd, &subData, &m_indexBuffer);
}