// Constants.h
// Christopher Ball 2016
// This is where I keep my constant variables for use throughout my project

#ifndef _CONSTANTS_H_
#define	_CONSTANTS_H_

#include "pch.h"

// Global Constants
namespace GlobalConstants
{
	const unsigned int	GAME_WIDTH		= 1280;							// width of game window in pixels
	const unsigned int	GAME_HEIGHT		= 720;							// height of game window in pixels
	const wchar_t		WND_CLASS_NAME[] = L"MyWndClass";				// name of class for creating a window
	const wchar_t		WINDOW_NAME[] = L"Assignment 507";				// Title of window that shows in top bar
	const float			BACK_COLOUR[4] = {0.3f, 0.3f, 0.3f, 0.0f };		// colour of window backPath

	// cell dimensions
	const unsigned int CELL_WIDTH = 2;	// width of wall
}

#endif _CONSTANTS_H_
