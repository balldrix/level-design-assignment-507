// SkyBox.h
// Christopher Ball 2016
// The sky box object for rendering the sky

#ifndef _SKYBOX_H_
#define _SKYBOX_H_

#include "GameObject.h"

// inherit from game object
class SkyBox : public GameObject
{
public:
	SkyBox();
	~SkyBox();
	void Update(); // update world position matrix
};

#endif _SKYBOX_H_