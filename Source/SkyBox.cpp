#include "SkyBox.h"

SkyBox::SkyBox()
{
}

SkyBox::~SkyBox()
{
}

void
SkyBox::Update()
{
	// scale and translate skybox
	m_scaleMatrix = XMMatrixScaling(500.0f, 500.0f, 500.0f);
	m_translationMatrix = XMMatrixTranslationFromVector(m_position);
	m_modelMatrix = m_scaleMatrix * m_translationMatrix;
}