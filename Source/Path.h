// Path.h
// Christopher Ball 2016
// Object class for the Path

#ifndef _PATH_H_
#define _PATH_H_

#include "GameObject.h"

// inherit from game object
class Path : public GameObject
{
public:
	Path();
	~Path();
	void Update(); // update world position matrix
};

#endif _PATH_H_
