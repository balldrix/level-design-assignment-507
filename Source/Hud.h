// Hud.h
// Christopher Ball 2016
// Container for all hud elements like 
// player's gun and shooting recharge bar

#ifndef _HUD_H_
#define _HUD_H_

#include "pch.h"
#include "Sprite.h"

// forward declarations
class Graphics;

class Hud
{
public:
	Hud();
	~Hud();
	void	Init(Graphics* graphics, SpriteBatch* spriteBatch); // initialise hud
	void	Update(float deltaTime);
	void	Render(SpriteBatch* spriteBatch);
			
	void	ReleaseAll();		// release all sprites
	void	DeleteAll();		// delete pointers
			
	void	MuzzleFlash();		// fire weapon
	void	ChargeWeapon(float chargeAmt); // charge up weapon meter
			
	void	SetChargeUp();		// toggle charge pickup notification
	void	SetDoubleDMG();		// toggle double damage notification
	void	SetRapidFire();		// toggle rapid fire notification

	bool	IsChargeUp() const { return m_chargeUp->GetActive(); } // is charge up active	
	bool	IsDoubleDMG() const { return m_doubleDMG->GetActive(); } // is doubledmg active
	bool	IsRapidFire() const { return m_rapidFire->GetActive(); } // is rapid fire active

private:
	Sprite*	m_energygun;		// player's weapon sprite
	Sprite*	m_flash;			// muzzle flash
	Sprite* m_chargeBackground; // weapon chargebar background
	Sprite* m_chargeBar;		// weapon chargebar
	Sprite* m_chargeBorder;		// weapon chargebar border
	Sprite*	m_rapidFireBar;		// weapon charge bar during rapid fire
	Sprite* m_chargeUp;			// weapon charge pickup notification
	Sprite* m_doubleDMG;		// double damage pickup up notification
	Sprite* m_rapidFire;		// rapid fire pickup notification
	Sprite* m_shootButton;		// button icon for shooting weapon
	Sprite* m_arrowUp;			// up arrow for moving forward
	Sprite* m_arrowDown;		// down arrow for moving down
	Sprite* m_arrowLeft;		// left arrow for turning left
	Sprite* m_arrowRight;		// right arrow for turning right

	std::vector<Sprite*> m_notifications; // pickup notification container

	float screenWidth;			// screen width
	float screenHeight;			// screen height

	float	m_flashTimer;		// muzzle flash timer
};

#endif _HUD_H_
