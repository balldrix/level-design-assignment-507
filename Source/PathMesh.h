// PathMesh.h
// Christopher Ball 2016
// Mesh information for the Path

#ifndef _PATHMESH_H_
#define _PATHMESH_H_

#include "MeshObject.h"

// forward declarations
class Graphics;

// inherit from game object
class PathMesh : public MeshObject
{
public:
	PathMesh();
	~PathMesh();
	void Init(Graphics* graphics); // initilalise path mesh
};

#endif _PATHMESH_H_