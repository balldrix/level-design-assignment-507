#include "GameObject.h"
#include "Graphics.h"
#include "MeshObject.h"
#include "Material.h"
#include "Constants.h"
#include "Shadow.h"

GameObject::GameObject() :
	m_ID(L"GameObject"),
	m_mesh(nullptr),
	m_material(nullptr),
	m_modelMatrix(XMMatrixIdentity()),
	m_scaleMatrix(XMMatrixIdentity()),
	m_translationMatrix(XMMatrixIdentity()),
	m_rotationalMatrix(XMMatrixIdentity()),
	m_animMatrix(XMMatrixIdentity()),
	m_position(0.0f, 0.0f, 0.0f),
	m_velocity(0.0f, 0.0f, 0.0f),
	m_defaultXAxis(0.0f, 0.0f, 0.0f),
	m_defaultYAxis(0.0f, 0.0f, 0.0f),
	m_defaultZAxis(0.0f, 0.0f, 0.0f),
	m_shadow(nullptr),
	m_hitbox(AABB(Vector3(0.0f,0.0f,0.0f), Vector3(0.0f, 0.0f, 0.0f)))
{
}

GameObject::~GameObject()
{
}

void
GameObject::Init(MeshObject* mesh, 
				 Material* material, 
				 Vector3 position)
{
	// set member variables with passed params
	m_mesh = mesh;
	m_material = material;
	m_position = position;

	// set default axis
	m_defaultXAxis = Vector3(1.0f, 0.0f, 0.0f);
	m_defaultYAxis = Vector3(0.0f, 1.0f, 0.0f);
	m_defaultZAxis = Vector3(0.0f, 0.0f, 1.0f);

	// set aabb
	m_hitbox.SetAABB(Vector3(-1.0f, -1.0f, -1.0f), Vector3(1.0f, 1.0f, 1.0f));
}

void
GameObject::Render(Graphics* graphics)
{
	if(graphics)
	{
		// if graphics exists
		// set world matrix
		graphics->SetWorldMatrix(m_modelMatrix);
		graphics->SetUVTransform(m_animMatrix);
	}

	// if the object has a material
	if(m_material)
	{
		m_material->RenderSetup(graphics);
	}

	// if object has a mesh
	if(m_mesh)
	{
		m_mesh->Render(graphics);
	}

	if(m_shadow) // check the npc has a shadow
	{
		// update shadow position
		m_shadow->RotationalMatrix(m_rotationalMatrix);
		m_shadow->SetPosition(m_position);
		m_shadow->Update();
	}
}

void
GameObject::SetID(const wchar_t* string)
{
	m_ID = string;
}

void
GameObject::SetPosition(Vector3 position)
{
	m_position = position;
}

void 
GameObject::SetVelocity(Vector3 velocity)
{
	m_velocity = velocity;
}

void 
GameObject::SetShadow(Shadow* shadow)
{
	m_shadow = shadow;
}

void 
GameObject::RenderShadow(Graphics* graphics)
{
	// if there is a shadow object attached then render it
	if(m_shadow)
	{
		m_shadow->Render(graphics);
	}
}