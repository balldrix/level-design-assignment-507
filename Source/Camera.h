// Camera.h
// Christopher Ball 2016
// manages camera position, view
// direction and frustrum

#ifndef _CAMERA_H_
#define _CAMERA_H_

#include "pch.h"

class Camera
{
public:
	Camera();
	~Camera();

	void Init(Vector3 position, float width, float height, float nearZ, float farZ); // initialise camera

	void LookAt(Vector3 facing); // look in direction of facing vector
	void Update(); // update camera

	void SetPosition(Vector3 position); // camera position
	void SetFacingDirection(Vector3 facing); // set camera facing direciton
	void SetUpDirection(Vector3 up); // set camera up direction
	void SetViewMatrix(); // set view matrix
	void SetProjectionMatrix(); // set projection matrix
	void SetView(Vector3 position, Vector3 facing, Vector3 up);
	void SetPerspective(float fov, float aspect, float nearZ, float farZ);

	Vector3 GetPosition()  { return m_position; } // get camera position
	Vector3 GetFacingDirection()  { return m_facingDirection; } // get facing direction 
	Vector3 GetUpDirection()  { return m_upDirection; } // get up direction 
	Matrix	GetViewMatrix() const { return m_viewMatrix; } // get view matrix
	Matrix	GetProjectionMatrix() const { return m_projectionMatrix; } // get projection matrix

private:
	Vector3 m_position; // camera position in space
	Vector3 m_facingDirection; // unit vector of camera's facing direction
	Vector3 m_upDirection; // unit vector of camera's upwards direction
	Matrix	m_viewMatrix; // camera view matrix
	Matrix	m_projectionMatrix; // camera projection frustrum matrix
	float	m_screenWidth; // screen width
	float	m_screenHeight; // screen height
	float	m_fov; // field of view angle
	float	m_aspect; // aspect ration
	float	m_nearZ; // near z position
	float	m_farZ;	// far z position
};

#endif _CAMERA_H_
