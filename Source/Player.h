// Player.h
// Christopher Ball 2016
// class for all player related methods and data

#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "pch.h"
#include "GameObject.h"

// player constants
const AABB PLAYER_HIT_BOX = AABB(Vector3(-0.2f, -1.0f, -0.2f), Vector3(0.2f, 1.0f, 0.2f)); // hit box
const Vector3 HEAD_OFFSET = Vector3(0.0f, 0.4f, 0.0f);	// head position is offset from origin of player position

// inherit from game object
class Player : public GameObject
{
public:
	Player();
	~Player();
	void			Init();							// initialtise
	void			Update(float deltaTime);		// update player
	
	void			SetFacing(Vector3 facing);		// set facing direction
	void			SetGridPosX(unsigned int x);	// set grid x pos
	void			SetGridPosZ(unsigned int z);	// set grid z pos
	
	unsigned int	GetGridPosX() const { return m_positionX; }		
	unsigned int	GetGridPosZ() const { return m_positionZ; }

	Vector3			GetFacing() const { return m_facing; } // get facing direction

	void			StepForward();					// move 1 square forward
	void			StepBackward();					// move 1 square backwards
	void			TurnRight();					// rotate 90 degrees right
	void			TurnLeft();						// rotate 90 degrees left
	
private:
	unsigned int	m_positionX;					// player grid position x
	unsigned int	m_positionZ;					// player grid position z
	Vector3			m_facing;						// player facing direction
	float			m_angle;						// rotational angle
};

#endif _PLAYER_H_