#include "Camera.h"

Camera::Camera() :
m_position(0.0f, 0.0f, 0.0f),
m_facingDirection(0.0f, 0.0f, 0.0f),
m_upDirection(0.0f, 0.0f, 0.0f),
m_viewMatrix(),
m_projectionMatrix(),
m_screenWidth(0.0f),
m_screenHeight(0.0f),
m_fov(0.0f),
m_aspect(0.0f),
m_nearZ(0.0f),
m_farZ(0.0f)
{}

Camera::~Camera()
{}

void 
Camera::Init(Vector3 position, float width, float height, float nearZ, float farZ)
{
	// set default camera values
	SetView(position, Vector3(0.0f, 0.0f, 1.0f), Vector3(0.0f, 1.0f, 0.0f));
	LookAt(m_facingDirection);

	// copy params into member variables
	m_screenWidth = width;
	m_screenHeight = height;
	m_nearZ = nearZ;
	m_farZ = farZ;
	m_fov = XM_PIDIV4; // Pi / 4 = 45 degrees
	m_aspect =  width / height; // ratio of width / height

	// set projection
	SetPerspective(m_fov, m_aspect, m_nearZ, m_farZ);
}

void
Camera::LookAt(Vector3 facing)
{
	// the look at must be facing ahead of the camera position
	Vector3 lookAt;
	lookAt.x = m_position.x + facing.x;
	lookAt.y = m_position.y + facing.y;
	lookAt.z = m_position.z + facing.z;
	SetView(m_position, lookAt, m_upDirection); // set view position, facing and up
}

void
Camera::Update()
{
	SetViewMatrix(); // set view matrix
	SetProjectionMatrix(); // set projection matrix
}

void 
Camera::SetPosition(Vector3 position)
{
	m_position = position;
}

void 
Camera::SetFacingDirection(Vector3 facing)
{
	m_facingDirection = facing;
}

void 
Camera::SetUpDirection(Vector3 up)
{
	m_upDirection = up;
}

void Camera::SetViewMatrix()
{
	// set view matrix with XMMatrixLookAtLH function 
	m_viewMatrix = XMMatrixLookAtLH(m_position, m_facingDirection, m_upDirection);
}

void 
Camera::SetProjectionMatrix()
{
	// set frustrum projection with XMMatrixPerspectiveFovLH function
	m_projectionMatrix = XMMatrixPerspectiveFovLH(m_fov, m_aspect, m_nearZ, m_farZ);
}

void
Camera::SetView(Vector3 position, Vector3 facing, Vector3 up)
{
	// set position, facing and up values
	m_position = position;
	m_facingDirection = facing;
	m_upDirection = up;
}

void 
Camera::SetPerspective(float fov, float aspect, float nearZ, float farZ)
{
	// set field of view, aspect ratio, near plane z and far plane z values
	m_fov = fov;
	m_aspect = aspect;
	m_nearZ = nearZ;
	m_farZ = farZ;
}


