#include "MazeBlock.h"

MazeBlock::MazeBlock()
{
}

MazeBlock::~MazeBlock()
{
}

void MazeBlock::Update()
{
	// set up bounding box
	m_hitbox.SetAABB(Vector3(-1.0f, -1.0f, -1.0f), Vector3(1.0f, 1.0f, 1.0));
	m_hitbox.OffSetAABB(m_position);

	// translate model position
	m_modelMatrix = XMMatrixTranslationFromVector(m_position);
}
