#include "Hud.h"
#include "Graphics.h"
#include "Sprite.h"

Hud::Hud() :
m_energygun(nullptr),
m_flash(nullptr),
m_chargeBackground(nullptr),
m_chargeBar(nullptr),
m_chargeBorder(nullptr),
m_rapidFireBar(nullptr),
m_chargeUp(nullptr),
m_doubleDMG(nullptr),
m_rapidFire(nullptr),
m_shootButton(nullptr),
m_arrowUp(nullptr),
m_arrowDown(nullptr),
m_arrowLeft(nullptr),
m_arrowRight(nullptr),
screenWidth(0.0f),
screenHeight(0.0f),
m_flashTimer(0.0f)
{
}

Hud::~Hud()
{
	ReleaseAll();
	DeleteAll();
}

void
Hud::Init(Graphics* graphics, SpriteBatch* spriteBatch)
{	
	// dimensions of screen in pixels
	screenWidth = graphics->GetWidth();
	screenHeight = graphics->GetHeight();

	// create memory for sprites
	m_energygun = new Sprite();
	m_flash = new Sprite();
	m_chargeBackground = new Sprite();
	m_chargeBorder = new Sprite();
	m_chargeBar = new Sprite();
	m_rapidFireBar = new Sprite();
	m_chargeUp = new Sprite();
	m_doubleDMG = new Sprite();
	m_rapidFire = new Sprite();
	m_shootButton = new Sprite();
	m_arrowUp = new Sprite();
	m_arrowDown = new Sprite();
	m_arrowLeft = new Sprite();
	m_arrowRight = new Sprite();

	// load sprite textures
	m_energygun->Init(graphics, "Assets\\Sprites\\pistol.dds");
	m_flash->Init(graphics, "Assets\\Sprites\\muzzleflash.dds");
	m_chargeBackground->Init(graphics, "Assets\\Sprites\\weaponchargeempty.dds");
	m_chargeBorder->Init(graphics, "Assets\\Sprites\\weaponchargeborder.dds");
	m_chargeBar->Init(graphics, "Assets\\Sprites\\weaponcharge.dds");
	m_rapidFireBar->Init(graphics, "Assets\\Sprites\\rapidfirebar.dds");
	m_chargeUp->Init(graphics, "Assets\\Sprites\\chargeupsprite.dds");
	m_doubleDMG->Init(graphics, "Assets\\Sprites\\doubledmgsprite.dds");
	m_rapidFire->Init(graphics, "Assets\\Sprites\\rapidfiresprite.dds");

	m_shootButton->Init(graphics, "Assets\\Sprites\\shootbutton.dds");
	m_arrowUp->Init(graphics, "Assets\\Sprites\\uparrow.dds");
	m_arrowDown->Init(graphics, "Assets\\Sprites\\downarrow.dds");
	m_arrowLeft->Init(graphics, "Assets\\Sprites\\leftarrow.dds");
	m_arrowRight->Init(graphics, "Assets\\Sprites\\rightarrow.dds");
		
	// set sprite positions
	m_energygun->SetPosition(Vector2(screenWidth * 0.6f, screenHeight * 0.85f));
	m_flash->SetPosition(m_energygun->GetPosition());
	m_chargeBackground->SetPosition(Vector2(screenWidth * 0.95f, screenHeight * 0.8f));
	m_chargeBorder->SetPosition(m_chargeBackground->GetPosition());
	m_chargeBar->SetPosition(m_chargeBackground->GetPosition());
	m_rapidFireBar->SetPosition(m_chargeBackground->GetPosition());
	m_shootButton->SetPosition(Vector2(screenWidth * 0.855f, screenHeight * 0.8f));
	m_arrowUp->SetPosition(Vector2(screenWidth * 0.15f, screenHeight * 0.66f));
	m_arrowDown->SetPosition(Vector2(screenWidth * 0.15f, screenHeight * 0.89f));
	m_arrowLeft->SetPosition(Vector2(screenWidth * 0.09f, screenHeight * 0.77f));
	m_arrowRight->SetPosition(Vector2(screenWidth * 0.21f, screenHeight * 0.77f));
	
	// pickup notifications' positions are set in the render method
	// due to the order changes

	// set default active sprites
	m_energygun->SetActive(true);	
	m_chargeBackground->SetActive(true);
	m_chargeBorder->SetActive(true);
	m_chargeBar->SetActive(true);
	m_shootButton->SetActive(true);
	m_arrowUp->SetActive(true);
	m_arrowDown->SetActive(true);
	m_arrowLeft->SetActive(true);
	m_arrowRight->SetActive(true);
}

void 
Hud::Update(float deltaTime)
{
	if(m_flash->GetActive())
	{
		if(m_flashTimer < 0) // if timer runs out
		{
			// reset timer and set flash to inactive
			m_flash->SetActive(false);
		}

		m_flashTimer -= deltaTime; // count down
	}
}

void 
Hud::Render(SpriteBatch* spriteBatch)
{
	// if energy gun is active
	if(m_energygun->GetActive())
	{
		// if muzzle flash if active
		if(m_flash->GetActive())
		{
			// render muzzle flash
			m_flash->Render(spriteBatch);
		}
			// render energy gun
		m_energygun->Render(spriteBatch);
	}

	// if rapid fire is active
	if(m_shootButton->GetActive())
	{
		// render rapid fire bar
		m_shootButton->Render(spriteBatch);
	}

	// if rapid fire is active
	if(m_rapidFireBar->GetActive())
	{
		// render rapid fire bar
		m_rapidFireBar->Render(spriteBatch);
	}

	// if charge bar is active
	if(m_chargeBar->GetActive())
	{
		// render chargebar
		m_chargeBar->Render(spriteBatch);
	}

	// if chargebackground is active
	if(m_chargeBackground->GetActive())
	{
		// render charge background
		m_chargeBackground->Render(spriteBatch);
	}

	// if chargebar border is active
	if(m_chargeBorder->GetActive())
	{
		// render charge border
		m_chargeBorder->Render(spriteBatch);
	}

	// if up arrow is active
	if(m_arrowUp->GetActive())
	{
		// render up arrow
		m_arrowUp->Render(spriteBatch);
	}

	// if down arrow is active
	if(m_arrowDown->GetActive())
	{
		// render down arrow
		m_arrowDown->Render(spriteBatch);
	}

	// if left arrow is active
	if(m_arrowLeft->GetActive())
	{
		// render left arrow
		m_arrowLeft->Render(spriteBatch);
	}

	// if right arrow is active
	if(m_arrowRight->GetActive())
	{
		// render right arrow
		m_arrowRight->Render(spriteBatch);
	}

	// loop through notification container
	for(unsigned int i = 0; i < m_notifications.size(); i++)
	{
		// set position and render
		m_notifications[i]->SetPosition(Vector2(screenWidth * 0.2f, (screenHeight * 0.7f) - m_notifications[i]->GetHeight() * i));
		m_notifications[i]->Render(spriteBatch);
	}
}

void 
Hud::ReleaseAll()
{
	// release all objects
	if(m_arrowRight) { m_rapidFire->Release(); }
	if(m_arrowLeft) { m_arrowLeft->Release(); }
	if(m_arrowDown) { m_arrowDown->Release(); }
	if(m_arrowUp) { m_arrowUp->Release(); }
	if(m_shootButton) { m_shootButton->Release(); }
	if(m_rapidFire) { m_rapidFire->Release(); }
	if(m_doubleDMG) { m_doubleDMG->Release(); }
	if(m_chargeUp) { m_chargeUp->Release(); }
	if(m_rapidFireBar) { m_rapidFireBar->Release(); }
	if(m_chargeBar) { m_chargeBar->Release(); }
	if(m_chargeBorder) { m_chargeBorder->Release(); }
	if(m_chargeBackground) { m_chargeBackground->Release(); }
	if(m_flash) { m_flash->Release(); }
	if(m_energygun) { m_energygun->Release(); }
}

void
Hud::DeleteAll()
{
	// clear notifications container
	m_notifications.clear();

	// delete right arrow button
	if(m_arrowRight)
	{
		delete m_arrowRight;
		m_arrowRight = nullptr;
	}

	// delete left arrow button
	if(m_arrowLeft)
	{
		delete m_arrowLeft;
		m_arrowLeft = nullptr;
	}

	// delete down arrow button
	if(m_arrowDown)
	{
		delete m_arrowDown;
		m_arrowDown = nullptr;
	}

	// delete up arrow button
	if(m_arrowUp)
	{
		delete m_arrowUp;
		m_arrowUp = nullptr;
	}

	// delete shoot button
	if(m_shootButton)
	{
		delete m_shootButton;
		m_shootButton = nullptr;
	}

	// delete rapid fire notification
	if(m_rapidFire)
	{
		delete m_rapidFire;
		m_rapidFire = nullptr;
	}

	// delete double damage notification
	if(m_doubleDMG)
	{
		delete m_doubleDMG;
		m_doubleDMG = nullptr;
	}

	// delete charge up notification
	if(m_chargeUp)
	{
		delete m_chargeUp;
		m_chargeUp = nullptr;
	}

	// delete rapid fire charge bar
	if(m_rapidFireBar)
	{
		delete m_rapidFireBar;
		m_rapidFireBar = nullptr;
	}

	// delete chargebar border
	if(m_chargeBorder)
	{
		delete m_chargeBorder;
		m_chargeBorder = nullptr;
	}

	// delete charge bar
	if(m_chargeBar)
	{
		delete m_chargeBar;
		m_chargeBar = nullptr;
	}

	// delete chargebar background
	if(m_chargeBackground)
	{
		delete m_chargeBackground;
		m_chargeBackground = nullptr;
	}

	// delete muzzle flash
	if(m_flash)
	{
		delete m_flash;
		m_flash = nullptr;
	}

	// delete energy gun sprite
	if(m_energygun)
	{
		delete m_energygun;
		m_energygun = nullptr;
	}
}

void Hud::MuzzleFlash()
{
	m_flash->SetActive(true); // set muzzle flash to active
	m_flashTimer = 0.05f; // reset muzzle flash timer
}

void Hud::ChargeWeapon(float chargeAmt)
{
	if(!m_rapidFire->GetActive())
	{
		// set sprites to active for charge bar
		m_chargeBackground->SetActive(true);
		m_chargeBar->SetActive(true);
		m_rapidFireBar->SetActive(false);

		// get current charge bar rect
		RECT r;
		r = m_chargeBackground->GetRect();

		// set top to fraction of bottom
		r.top = m_chargeBackground->GetHeight() * (1.0f - chargeAmt);

		// set rect
		m_chargeBackground->SetRect(r);
	}
	else
	{
		// set correct charge bars
		m_chargeBackground->SetActive(false);
		m_chargeBar->SetActive(false);
		m_rapidFireBar->SetActive(true);
	}
}

void
Hud::SetChargeUp()
{
	// if sprite is active then switch off
	// else switch on
	if(m_chargeUp->GetActive())
	{
		// set to inactive and remove from container
		m_chargeUp->SetActive(false);
		m_notifications.erase(std::remove(m_notifications.begin(), m_notifications.end(), m_chargeUp), m_notifications.end());
	}
	else
	{
		// set sprite to active and add to container
		m_chargeUp->SetActive(true);
		m_notifications.push_back(m_chargeUp);
	}
}

void
Hud::SetDoubleDMG()
{
	// if sprite is active then switch off
	// else switch on
	if(m_doubleDMG->GetActive())
	{
		// set to inactive and remove from container
		m_doubleDMG->SetActive(false);
		m_notifications.erase(std::remove(m_notifications.begin(), m_notifications.end(), m_doubleDMG), m_notifications.end());
	}
	else
	{
		// set sprite to active and add to container
		m_doubleDMG->SetActive(true);
		m_notifications.push_back(m_doubleDMG);
	}
}

void
Hud::SetRapidFire()
{
	// if sprite is active then switch off
	// else switch on
	if(m_rapidFire->GetActive())
	{
		// set to inactive and remove from container
		m_rapidFire->SetActive(false);
		m_notifications.erase(std::remove(m_notifications.begin(), m_notifications.end(), m_rapidFire), m_notifications.end());
	}
	else
	{
		// set sprite to active and add to container
		m_rapidFire->SetActive(true);
		m_notifications.push_back(m_rapidFire);
	}
}
