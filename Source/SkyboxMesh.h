// SkyBoxMesh.h
// Christopher Ball 2016
// Mesh for the skybox

#ifndef _SKYBOXMESH_H_
#define _SKYBOXMESH_H_

#include "MeshObject.h"

// forward declarations
class Graphics;

class SkyBoxMesh : public MeshObject
{
public:
	SkyBoxMesh();
	~SkyBoxMesh();
	void Init(Graphics* graphics);
};

#endif _SKYBOXMESH_H_