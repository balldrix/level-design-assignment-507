// Material.h
// Christopher Ball 2016
// Material class to hold data related 
// to textures, lighting and shaders

#ifndef _MATERIAL_H_
#define _MATERIAL_H_

#include "pch.h"

// forward declarations
class Graphics;
class Texture;
class Shader;

class Material
{
public:
	Material();
	~Material();
	void Init(Texture* texture, Shader* shader); // initialises material
	void RenderSetup(Graphics* graphics); // prepare pipeline for rendering materials

private:
	ID3D11ShaderResourceView* m_texture; // Shader Resource View
	ID3D11VertexShader* m_vertexShader; // vertex shader
	ID3D11PixelShader* m_pixelShader; // pixel shader
	ID3D11InputLayout* m_inputLayout; // vertex input layout
};

#endif _MATERIAL_H_
