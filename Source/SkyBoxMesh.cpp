#include "SkyBoxMesh.h"
#include "Graphics.h"
#include "ConstantBuffers.h"

SkyBoxMesh::SkyBoxMesh()
{
}

SkyBoxMesh::~SkyBoxMesh()
{
}

void
SkyBoxMesh::Init(Graphics* graphics)
{
	// setup wall mesh vertices, indices
	// and buffers for the wall mesh

	D3D11_BUFFER_DESC bd = { 0 };
	D3D11_SUBRESOURCE_DATA subData = { 0 };

	Vertex cubeVerts[] =
	{
		{ Vector3(-1.0f, 1.0f, -1.0f), Vector2(1.0f, 0.0f) },
		{ Vector3(1.0f, 1.0f, -1.0f), Vector2(0.0f, 0.0f) },
		{ Vector3(1.0f, 1.0f, 1.0f), Vector2(0.0f, 1.0f) },
		{ Vector3(-1.0f, 1.0f, 1.0f), Vector2(1.0f, 1.0f) },

		{ Vector3(-1.0f, -1.0f, -1.0f), Vector2(0.0f, 0.0f) },
		{ Vector3(1.0f, -1.0f, -1.0f), Vector2(1.0f, 0.0f) },
		{ Vector3(1.0f, -1.0f, 1.0f), Vector2(1.0f, 1.0f) },
		{ Vector3(-1.0f, -1.0f, 1.0f), Vector2(0.0f, 1.0f) },

		{ Vector3(-1.0f, -1.0f, 1.0f), Vector2(0.0f, 1.0f) },
		{ Vector3(-1.0f, -1.0f, -1.0f), Vector2(1.0f, 1.0f) },
		{ Vector3(-1.0f, 1.0f, -1.0f), Vector2(1.0f, 0.0f) },
		{ Vector3(-1.0f, 1.0f, 1.0f), Vector2(0.0f, 0.0f) },

		{ Vector3(1.0f, -1.0f, 1.0f), Vector2(1.0f, 1.0f) },
		{ Vector3(1.0f, -1.0f, -1.0f), Vector2(0.0f, 1.0f) },
		{ Vector3(1.0f, 1.0f, -1.0f), Vector2(0.0f, 0.0f) },
		{ Vector3(1.0f, 1.0f, 1.0f), Vector2(1.0f, 0.0f) },

		{ Vector3(-1.0f, -1.0f, -1.0f), Vector2(0.0f, 1.0f) },
		{ Vector3(1.0f, -1.0f, -1.0f), Vector2(1.0f, 1.0f) },
		{ Vector3(1.0f, 1.0f, -1.0f), Vector2(1.0f, 0.0f) },
		{ Vector3(-1.0f, 1.0f, -1.0f), Vector2(0.0f, 0.0f) },

		{ Vector3(-1.0f, -1.0f, 1.0f), Vector2(1.0f, 1.0f) },
		{ Vector3(1.0f, -1.0f, 1.0f), Vector2(0.0f, 1.0f) },
		{ Vector3(1.0f, 1.0f, 1.0f), Vector2(0.0f, 0.0f) },
		{ Vector3(-1.0f, 1.0f, 1.0f), Vector2(1.0f, 0.0f) },
	};

	// create index buffer
	WORD cubeIndices[] =
	{
		0,1,3,
		3,1,2,

		5,4,6,
		6,4,7,

		8,9,11,
		11,9,10,

		13,12,14,
		14,12,15,

		16,17,19,
		19,17,18,

		21,20,22,
		22,20,23
	};

	m_vertexCount = 8;
	m_indexCount = ARRAYSIZE(cubeIndices);

	// create vertex buffer for a cube
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(Vertex) * ARRAYSIZE(cubeVerts);
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	subData = { cubeVerts, 0, 0 };
	graphics->GetDevice()->CreateBuffer(&bd, &subData, &m_vertexBuffer);

	// create index buffer
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(short) * m_indexCount;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	subData = { cubeIndices, 0, 0 };
	graphics->GetDevice()->CreateBuffer(&bd, &subData, &m_indexBuffer);
}
