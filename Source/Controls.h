// Controls.h
// Christopher Ball 2016
// list of controls

#ifndef _CONTROLS_H_
#define _CONTROLS_H_

#include "pch.h"

// namespace to turn virtual key controls
// into readable name
namespace Controls
{
	UCHAR escKey = VK_ESCAPE;
	UCHAR playerStepFowards = VK_UP;
	UCHAR playerStepBackwards = VK_DOWN;
	UCHAR playerTurnLeft = VK_LEFT;
	UCHAR playerTurnRight = VK_RIGHT;
	UCHAR shoot = VK_SPACE;
}

#endif _CONTROLS_H_
