// PlayerWeapon.h
// Christopher Ball 2016
// struct containing player weapon data

#ifndef _PLAYER_WEAPON_H_
#define _PLAYER_WEAPON_H

struct PlayerWeapon
{
	int		damage; // damage weapon can inflict
	float	range; // range of gun
	float	timer; // weapon charge timer
	float	chargeAmt; // weapon charge time
	bool	canShoot; // can shoot when weapon has charged
};

#endif _PLAYER_WEAPON_H_
