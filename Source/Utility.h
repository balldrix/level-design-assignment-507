// Utility.h
// Christopher Ball 2016
// header containing useful global
// utility functions

#ifndef _UTILITY_H_
#define _UTILITY_H_

#include "pch.h"

// utility namespace for global functions
namespace Utility
{
	Vector3 GridToWorldSpace(unsigned int x, unsigned int z); // convert map grid to world space
}

#endif _UTILITY_H_
