// ConstantBuffers.h
// Christopher Ball 2016
// header file for keeping Constant Buffer and 
// vertex structs, for the shaders, in one place

#ifndef _CONSTANTBUFFERS_H_
#define	_CONSTANTBUFFERS_H_

#include "pch.h"

// Struct for cube vertices
struct Vertex
{
	Vector3 position;	// vertex position
	Vector2 uv;			// texture co-ordinates
};

// constant buffer to be passed to Vertex Shader
struct ConstantBuffer
{
	Matrix m_finalMatrix; // final calculated matrix for the vertex shader
};

// constant buffer for UV translation
// used for texture animation in vertex shader
struct TextureTranslation
{
	Matrix m_textTransMatrix; // buffer to translate UV coordinates
};

// basic vertex input layout definition
static D3D11_INPUT_ELEMENT_DESC inputElementDesc[] =
{
	{
		"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0
	},
	{
		"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0
	},
};

#endif _CONSTANTBUFFERS_H_