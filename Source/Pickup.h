// Pickup.h
// Christopher Ball 2016
// class for game pickups

#ifndef _PICKUP_H_
#define _PICKUP_H_

#include "GameObject.h"

// inherit from game object
class Pickup : public GameObject
{
public:
	Pickup();
	~Pickup();
	void Update(); // update world position

private:
	float m_scale; // scale of pickup size
};

#endif _PICKUP_H_