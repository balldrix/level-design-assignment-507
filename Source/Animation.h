// Animation.h
// Christopher Ball 2016
// Incomplete class, temporary structs to contain data
// this will eventualy be complete with frame animation methods

#ifndef _ANIMATION_H_
#define _ANIMATION_H_

struct SpriteSheet
{
	int frameWidth; // sprite sheet frame width
	int frameHeight; // sprite sheet frame height
	int sheetWidth; // sprite sheet width
	int sheetHeight; // sprite sheet height
	int columns; // number of columns in sheet
};

struct Animation
{
	float timer; // timer for animation length
	bool done; // is animation done
	bool loop; // is animation a loop
	int currentFrame; // current frame in animation
	int numFrames; // frames in current animation
};

#endif _ANIMATION_H_