// Game.h
// Christopher Ball 2016
// The game class manages the main game loop
// and loading, updating and rendering of
// all game assets and scenes

#ifndef _GAME_H_
#define _GAME_H_

#include "pch.h"
#include "Timer.h"
#include "Hud.h"
#include "PlayerWeapon.h"

// forward declarations
class Graphics;
class GameObject;
class Input;
class LevelManager;
class Camera;
class Player;
class Shader;
class Texture;
class Material;
class CubeMesh;
class MazeBlock;
class PathMesh;
class Path;
class QuadMesh;
class NPC;
class ShadowMesh;
class Shadow;
class SkyBoxMesh;
class SkyBox;
class Pickup;

// const strings for pickup ids
namespace PickupIDs
{
	const wchar_t CHARGE_UP[] = L"ChargeUp";
	const wchar_t DOUBLE_DAMAGE[] = L"DoubleDMG";
	const wchar_t RAPID_FIRE[] = L"RapidFire";
}

const float PICKUP_TIMER = 200.0f; // cool down time for pickups
const float WEAPON_CHARGE_TIME = 3.0f; // initial weapon charge time
const int WEAPON_DAMAGE = 100; // initial weapon damage
const int WEAPON_RANGE = 20;  // initial weapon range

const float CHARGE_UP_TIME = 0.3f; // charge up timer
const float RAPID_FIRE_TIME = 0.02f; // rapid fire charge time

class Game
{
public:
	Game();
	~Game();
	void			Init(Graphics* graphics);	// initialises game class and passes pointer to the graphics class

	void			Run(); // main game function

	void			ProcessInput(); // read inputs
	void			Update(float deltaTime); // update all objects in the scene
	void			Render(); // Render scene

	void			ReleaseAll(); // release all pointers
	void			DeleteAll(); // delete all pointers

	void			NewGame(); // set up new game at level 1
	void			LoadAssets(); // load all required game assets
	void			ChangeLevel(); // load next level
	void			ResetGame(); // reset game so player tries again
	void			EndGame(); // calls end game when player is caught

	void			SpawnMaze(); // set maze wall and path objects
	void			SpawnNPCs(); // set enemy npc positions and waypoints
	void			SpawnItems(); // set item positions
	void			DeleteMaze(); // delete maze wall and path objects
	void			DeleteNPCs(); // delete npcs
	void			DeleteItems(); // delete Items

	void			FireWeapon(); // calls all required methods when gun is fired

	void			ResetMouse(); // reset mouse pos to middle of game window

	bool			Raycast(const Vector3& start, const Vector3& end, GameObject object); // trace a line and return true if it intersects and object

	Vector2			GetMidScreenPos(); // return the pixel position of the middle of the game window
	bool			IsGridSpaceFree(unsigned int x, unsigned int z); // check the grid space is a path

	LRESULT			MessageHandler(HWND hWindow, UINT msg, WPARAM wParam, LPARAM lParam); // deals with windows messages

private:
	Graphics*		m_graphics; // pointer to graphics class for access to the gpu device
	Input*			m_input; // manages key and mouse inputs
	LevelManager*	m_levelManager; // keeps track of level num and load levels
	Player*			m_player; // player object
	Camera*			m_camera; // the main first person camera
	SpriteBatch*	m_spriteBatch; // 2d sprites engine

	Shader*			m_simpleShader; // basic normal shader 
	Shader*			m_skyBoxShader; // shader for skybox
	Shader*			m_spriteShader; // simple animated sprite

	Texture*		m_wallTexture; // texture for wall material
	Texture*		m_entryTexture; // texture for level entrance door
	Texture*		m_exitTexture; // texture for exit door
	Texture*		m_pathTexture; // texture for Path tiles
	Texture*		m_NPCTexture; // texture for ai material
	Texture*		m_chargeUpCrateTexture; // crate texture for charge up pickup
	Texture*		m_doubleDMGCrateTexture; // crate texture for double damage pickup
	Texture*		m_rapidFireCrateTexture; // crate texture for rapid fire pickup
	Texture*		m_shadowTexture; // blob shadow
	Texture*		m_skyBoxTexture; // sky box texture

	Material*		m_wallMaterial; // material for wall block	
	Material*		m_entryMaterial; // material for entrance door
	Material*		m_exitMaterial; // material for door block
	Material*		m_pathMaterial; // material for Path
	Material*		m_NPCMaterial; // material for ai
	Material*		m_chargeUpCrateMaterial; // crate material for weapon charge up pickup
	Material*		m_doubleDMGCrateMaterial; // crate material for doubld damage
	Material*		m_rapidFireCrateMaterial; // crate material for rapid fire pickup
	Material*		m_shadowMaterial; // blob shadow
	Material*		m_skyBoxMaterial; // sky box material

	CubeMesh*		m_cubeMesh; // mesh for maze wall blocks
	PathMesh*		m_pathMesh; // mesh for Path
	QuadMesh*		m_NPCMesh; // mesh for ai
	ShadowMesh*		m_shadowMesh;	// blob shadow mesh
	SkyBoxMesh*		m_skyBoxMesh; // sky box mesh

	MazeBlock*		m_wallBlock; // the block for maze wall
	MazeBlock*		m_entryBlock; // the block for level entrance
	MazeBlock*		m_exitBlock; // block for the exit
	Path*			m_path; // Path object

	NPC*			m_NPC; // ai object
	
	Shadow*			m_shadow; // blob shadow object
	
	SkyBox*			m_skyBox; // skybox object
	
	Pickup*			m_chargeUpPickup; // crate object faster weapon charge
	Pickup*			m_doubleDMGPickup; // crate object for double damage
	Pickup*			m_rapidFirePickup; // crate object for rapid fire 
	
	std::unique_ptr<AudioEngine> m_audio; // audio engine from directx tk
	SoundEffect*	m_energygunshot; // energy gun sound file
	SoundEffect*	m_NPCMovement; // npc movement sound
	SoundEffect*	m_NPCHit; // npc shot sound
	SoundEffect*	m_NPCDead; // npc dead sound
	std::unique_ptr<SoundEffectInstance> m_weaponSound; // weapon shot sound effect instance
	SoundEffect*	m_chargeUpSound; // sound when charge up is collected
	SoundEffect*	m_doubleDMGSound; // sound when double dmg is picked up
	SoundEffect*	m_rapidFireSound; // sound when rapid fire is picked up

	std::vector<GameObject*> m_mazeObjects; // vector list of wall objects
	std::vector<NPC*> m_NPCList; // list of ai objects
	std::vector<GameObject*> m_pickups; // container of pickup objects
	
	Hud				m_hud; // 2d hud overlay

	Timer			m_timer; // helper object for time step
	float			m_timerFreq; // timer frequency 
	float			m_currentTime; // tick time
	float			m_previousTime; // previous tick time

	bool			m_retryAudio; // sets if audio device is lost

	float			m_keyDelay; // delay for keypress
	bool			m_keyPressed; // key was pressed last frame

	PlayerWeapon	m_playerWeapon; // player weapon data	

	Vector2			m_startPosition; // player start grid position
	Vector2			m_finishPosition; // level finish grid position

	float			m_chargeUpTimer; // chargeup cooldown timer
	float			m_doubleDMGTimer; // double dmg cooldown timer
	float			m_rapidFireTimer; // rapid fire cooldown timer
};

#endif _GAME_H_
