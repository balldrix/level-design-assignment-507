// Level.h
// Christopher Ball 2016
// A class to manage the loading of the maze
// from a text file

#ifndef _LEVEL_H_
#define _LEVEL_H_

#include "pch.h"

// constants
const unsigned int MAZE_WIDTH = 23; // maze width
const unsigned int MAZE_DEPTH = 23; // maze depth

// cell values
namespace MapValues
{
	const int WALL = 3;			// 4 is a wall
	const int PATH = 2;			// 3 is a path
	const int ENTRANCE = 8;			// 8 is the door
	const int EXIT = 7;			// 7 is exit
	const int NPC	= 2;		// 5 is NPC
	const int START = 0;		// 0 is player spawn position
	const int FINISH = 1;		// 10 is the end of maze
	const int CHARGE_UP = 2;	// 11 is pickup for faster weapon cooldown
	const int DOUBLE_DAMAGE = 3;	// 12 is pickup for double damage
	const int RAPID_FIRE = 4;	// 13 is for Rapid fire pickup
}

struct NPCData
{
	int speed;	// NPC speed
	int hp;		// NPC hit points
};

class Level
{
public:
	Level();
	~Level(); 

	bool				Init(std::string mapFile, std::string itemsFile, std::string NPCPositionFile, std::string NPCDifficulty); // initialise level
	
	bool				LoadMaze(std::string filename); // load map grid from file into map array
	bool				LoadNPCPositions(std::string filenam); // load npc position data
	bool				LoadNPCData(std::string filename); // load npc difficulty data from file
	bool				LoadItemMap(std::string filename);

	char				GetMap(unsigned int x, unsigned int z) const { return m_map[(MAZE_WIDTH*z) + x]; } // return value of specific cell in 2d array
	char				GetNPCMap(unsigned int x, unsigned int z) const { return m_NPCMap[(MAZE_WIDTH*z) + x]; } // return value of gid position on npc map
	char				GetItemMap(unsigned int x, unsigned int z) const { return m_itemMap[(MAZE_WIDTH*z) + x]; } // return value in grid position on item map

	NPCData				GetNPCData() const	 { return m_NPCData; } // return npc data struct
	void				DeleteMaps(); // clears level containers

private:
	std::vector<int>	m_map;		 // array of maze map
	std::vector<int>	m_NPCMap;	 // array of NPC locations
	std::vector<int>	m_itemMap;	 // array of items

	NPCData				m_NPCData;	 // Data struct of NPC data
};

#endif _LEVEL_H_
