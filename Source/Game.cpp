#include "Game.h"

#include "pch.h"

#include "Graphics.h"
#include "Input.h"
#include "LevelManager.h"
#include "Player.h"
#include "Camera.h"

// we must specify the exact location for the audio libray as it's now a legacy directx sdk
#include <C:\Program Files (x86)\Microsoft DirectX SDK (June 2010)\Include\xaudio2.h> 

#include "Shader.h"
#include "Texture.h"
#include "Material.h"

#include "CubeMesh.h"
#include "PathMesh.h"
#include "QuadMesh.h"
#include "ShadowMesh.h"
#include "SkyBoxMesh.h"

#include "GameObject.h"
#include "MazeBlock.h"
#include "Path.h"
#include "NPC.h"
#include "Shadow.h"
#include "SkyBox.h"
#include "Pickup.h"

#include "Constants.h"
#include "ConstantBuffers.h"

#include "Controls.h"
#include "Utility.h"
#include "UnitVectors.h"
#include "Error.h"

Game::Game() :
m_graphics(nullptr),
m_input(nullptr),
m_levelManager(nullptr),
m_player(nullptr),
m_camera(nullptr),
m_spriteBatch(nullptr),
m_simpleShader(nullptr),
m_skyBoxShader(nullptr),
m_spriteShader(nullptr),
m_wallTexture(nullptr),
m_entryTexture(nullptr),
m_exitTexture(nullptr),
m_pathTexture(nullptr),
m_NPCTexture(nullptr),	
m_chargeUpCrateTexture(nullptr),
m_doubleDMGCrateTexture(nullptr),
m_rapidFireCrateTexture(nullptr),
m_shadowTexture(nullptr),
m_skyBoxTexture(nullptr),
m_wallMaterial(nullptr),
m_entryMaterial(nullptr),
m_exitMaterial(nullptr),
m_pathMaterial(nullptr),
m_NPCMaterial(nullptr),
m_chargeUpCrateMaterial(nullptr),
m_doubleDMGCrateMaterial(nullptr),
m_rapidFireCrateMaterial(nullptr),
m_shadowMaterial(nullptr),
m_skyBoxMaterial(nullptr),
m_cubeMesh(nullptr),
m_pathMesh(nullptr),
m_NPCMesh(nullptr),
m_shadowMesh(nullptr),
m_skyBoxMesh(nullptr),
m_wallBlock(nullptr),
m_entryBlock(nullptr),
m_exitBlock(nullptr),
m_path(nullptr),
m_NPC(nullptr),
m_shadow(nullptr),
m_skyBox(nullptr),
m_chargeUpPickup(nullptr),
m_doubleDMGPickup(nullptr),
m_rapidFirePickup(nullptr),
m_NPCMovement(nullptr),
m_NPCHit(nullptr),
m_NPCDead(nullptr),
m_chargeUpSound(nullptr),
m_doubleDMGSound(nullptr),
m_rapidFireSound(nullptr),
m_timerFreq(0.0f),
m_currentTime(0.0f),
m_previousTime(0.0f),
m_retryAudio(false),
m_keyDelay(0.0f),
m_keyPressed(false),
m_startPosition(Vector2::Zero),
m_finishPosition(Vector2::Zero),
m_chargeUpTimer(0.0f),
m_doubleDMGTimer(0.0f),
m_rapidFireTimer(0.0f)
{
}

Game::~Game()
{
	DeleteAll(); // delete all pointers
}

void
Game::Init(Graphics* graphics)
{
	m_graphics = graphics; // copy pointer address to 

	// create new input class
	m_input = new Input();

	// create new levelmanager
	m_levelManager = new LevelManager();

	// create new player object
	m_player = new Player();

	// create new first person camera
	m_camera = new Camera();

	// allow multi threading for audio engine
	CoInitializeEx(NULL, COINIT_MULTITHREADED);

	// set audio engine flags
	AUDIO_ENGINE_FLAGS eflags = AudioEngine_EnvironmentalReverb |
								AudioEngine_ReverbUseFilters |
								AudioEngine_UseMasteringLimiter;
	#ifdef _DEBUG
	eflags = eflags | AudioEngine_Debug;
	#endif

	// initialise audio engine
	m_audio.reset(new AudioEngine(eflags));
	m_audio->SetReverb(Reverb_Alley); // set reverb for 3d effects

	// initialse sprite batch engine
	m_spriteBatch = new SpriteBatch(m_graphics->GetDeviceContext());

	// load all assets
	LoadAssets();

	// start new game
	NewGame();
}

void
Game::Run()
{
	m_currentTime = m_timer.GetTicks(); // get cpu tick count
	float deltaTime = (m_currentTime - m_previousTime) * m_timerFreq; // calculate time taken since last update
	m_previousTime = m_currentTime; // keep current time for next update

	// if delta time becomes too large
	// lock at 60fps
	if(deltaTime > 0.016f)
	{
		deltaTime = 0.016f;
	}

	ProcessInput(); // read key and mouse input into game
	Update(deltaTime); // update game
	Render(); // render objects	
}

void 
Game::ProcessInput()
{
	// if esc key is pressed
	if(m_input->IsKeyDown(Controls::escKey))
	{
		PostQuitMessage(0); // quit game
	}

	// check key wasn't pressed last update
	if(!m_keyPressed)
	{
		// if player presses key to move forwards or backwards
		if(m_input->IsKeyDown(Controls::playerStepFowards))
		{
			// check if the space ahead is free
			if(IsGridSpaceFree(m_player->GetGridPosX() + (int)m_player->GetFacing().x,
			   m_player->GetGridPosZ() - (int)m_player->GetFacing().z))
			{
				m_player->StepForward(); // step forward
			}
			m_keyPressed = true; // key was pressed this frame
			m_keyDelay = 0.0f; // reset delay counter
		}
		else if(m_input->IsKeyDown(Controls::playerStepBackwards))
		{
			// check if the space behind is free
			if(IsGridSpaceFree(m_player->GetGridPosX() - (int)m_player->GetFacing().x,
			   m_player->GetGridPosZ() + (int)m_player->GetFacing().z))
			{
				m_player->StepBackward(); // step backward
			}
			m_keyPressed = true; // key was pressed this frame
			m_keyDelay = 0.0f; // reset delay counter
		}

		// if player presses key to move left or right
		if(m_input->IsKeyDown(Controls::playerTurnRight))
		{
			m_player->TurnRight(); // rotate 90 degrees right
			m_keyPressed = true; // key was pressed this frame
			m_keyDelay = 0.0f; // reset delay counter
		}
		else if(m_input->IsKeyDown(Controls::playerTurnLeft))
		{
			m_player->TurnLeft(); // rotate 90 degrees left
			m_keyPressed = true; // key was pressed this frame
			m_keyDelay = 0.0f; // reset delay counter
		}

		// if shoot button is pressed
		if(m_input->IsKeyDown(Controls::shoot))
		{
			// if player is allowed to shoot
			if(m_playerWeapon.canShoot)
			{
				FireWeapon();
			}
		}
	}

	// if player releases key to move
	if(!m_input->IsKeyDown(Controls::playerStepFowards) &&
	   !m_input->IsKeyDown(Controls::playerStepBackwards) &&
	   !m_input->IsKeyDown(Controls::playerTurnRight) &&
	   !m_input->IsKeyDown(Controls::playerTurnLeft))
	{
		m_keyPressed = false;
	}
}

void
Game::Update(float deltaTime)
{
	////////////////////////////////////////////////////////////////
	// player
	m_player->Update(deltaTime); 

	////////////////////////////////////////////////////////////////
	// camera 
	m_camera->SetPosition(m_player->GetPosition() + HEAD_OFFSET); // set camera position
	m_camera->LookAt(m_player->GetFacing());  // set view direction
	m_camera->SetPerspective(XM_PIDIV4, m_graphics->GetWidth() / m_graphics->GetHeight(), 0.1f, 100.0f); // set frustum
	m_camera->Update(); // update camera

	////////////////////////////////////////////////////////////////
	// NPC
	for(unsigned int i = 0; i < m_NPCList.size(); i++)
	{
		// update NPC position and states
		m_NPCList[i]->Update(deltaTime);

		// check NPC isn't dead
		if(m_NPCList[i]->GetState() != NPC_DEAD)
		{	// loop though maze objects
			for(unsigned int j = 0; j < m_mazeObjects.size(); j++)
			{	
				// check if object is a wall
				if(m_mazeObjects[j]->GetPosition().y == 0.0f)
					{
						// check raycast from NPC to player for maze objects in the way
						if(Raycast(m_NPCList[i]->GetPosition(),
						   m_player->GetPosition(),
						   *m_mazeObjects[j]))
						{
							// if ray hits maze object player is hidden
							// from NPC line of sight
							m_NPCList[i]->SetLineOfSight(false);
							break;
						}
						m_NPCList[i]->SetLineOfSight(true); // if no object is in the way player isn't hidden
					}
			}

			// loop through object list to check if NPC is getting too close to the walls
			for(unsigned int j = 0; j < m_mazeObjects.size(); j++)
			{
				// make sure we don't check the floor
				if(m_mazeObjects[j]->GetPosition().y == 0)
				{
					// set random move direction
					if(m_NPCList[i]->GetState() == NPC_ROAMING)
					{
						// while the NPC is too close to the wall in front
						// randomise it's facing direction until it's not facing a wall
 						while(Raycast(m_NPCList[i]->GetPosition(), m_NPCList[i]->GetPosition() + m_NPCList[i]->GetDirection(), *m_mazeObjects[j]))
						{
							m_NPCList[i]->SetRandomDirection();
						}
					}
					else
					{
						// check NPC isn't too close to wall
						// by checking in all eight compass directions
						// if the NPC is too close nudge the position in the opposite direction

						// check north
						if(Raycast(m_NPCList[i]->GetPosition(), m_NPCList[i]->GetPosition() + MapDirections::North, *m_mazeObjects[j]))
						{
							// nudge NPC south
							m_NPCList[i]->SetPosition(m_NPCList[i]->GetPosition() + (MapDirections::South * m_NPCList[i]->GetSpeed()) * deltaTime);
						}

						// check south
						if(Raycast(m_NPCList[i]->GetPosition(), m_NPCList[i]->GetPosition() + MapDirections::South, *m_mazeObjects[j]))
						{
							// nudge north
							m_NPCList[i]->SetPosition(m_NPCList[i]->GetPosition() + (MapDirections::North * m_NPCList[i]->GetSpeed()) * deltaTime);
						}

						// check east
						if(Raycast(m_NPCList[i]->GetPosition(), m_NPCList[i]->GetPosition() + MapDirections::East, *m_mazeObjects[j]))
						{
							// nudge west
							m_NPCList[i]->SetPosition(m_NPCList[i]->GetPosition() + (MapDirections::West * m_NPCList[i]->GetSpeed()) * deltaTime);
						}

						// check west
						if(Raycast(m_NPCList[i]->GetPosition(), m_NPCList[i]->GetPosition() + MapDirections::West, *m_mazeObjects[j]))
						{
							// nudge east
							m_NPCList[i]->SetPosition(m_NPCList[i]->GetPosition() + (MapDirections::East * m_NPCList[i]->GetSpeed()) * deltaTime);
						}

						// check north west
						if(Raycast(m_NPCList[i]->GetPosition(), m_NPCList[i]->GetPosition() + MapDirections::NorthWest, *m_mazeObjects[j]))
						{
							// nudge NPC south east
							m_NPCList[i]->SetPosition(m_NPCList[i]->GetPosition() + (MapDirections::SouthEast * m_NPCList[i]->GetSpeed()) * deltaTime);
						}

						// check south west
						if(Raycast(m_NPCList[i]->GetPosition(), m_NPCList[i]->GetPosition() + MapDirections::SouthWest, *m_mazeObjects[j]))
						{
							// nudge north east
							m_NPCList[i]->SetPosition(m_NPCList[i]->GetPosition() + (MapDirections::NorthEast * m_NPCList[i]->GetSpeed()) * deltaTime);
						}

						// check north east
						if(Raycast(m_NPCList[i]->GetPosition(), m_NPCList[i]->GetPosition() + MapDirections::NorthEast, *m_mazeObjects[j]))
						{
							// nudge south west
							m_NPCList[i]->SetPosition(m_NPCList[i]->GetPosition() + (MapDirections::SouthWest * m_NPCList[i]->GetSpeed()) * deltaTime);
						}

						// check south east
						if(Raycast(m_NPCList[i]->GetPosition(), m_NPCList[i]->GetPosition() + MapDirections::SouthEast, *m_mazeObjects[j]))
						{
							// nudge north west
							m_NPCList[i]->SetPosition(m_NPCList[i]->GetPosition() + (MapDirections::NorthWest * m_NPCList[i]->GetSpeed()) * deltaTime);
						}
					}
				}
			}

			// if npc catches player
			if((m_player->GetPosition() -
			   m_NPCList[i]->GetPosition()).Length() < 0.5f)
			{
				EndGame(); // end game screen
			}
		}
	}

	////////////////////////////////////////////////////////////////
	// pickups
	for(unsigned int i = 0; i < m_pickups.size(); i++)
	{
		// calculate distance of pickup to player
		float distanceToPlayer = 0.0f;
		distanceToPlayer = Vector3(m_player->GetPosition() - m_pickups[i]->GetPosition()).Length();

		// if player touches pickup
		// activate pickup benefits
		if(distanceToPlayer < 1.0f)
		{
			// check ID of pickup
			if(m_pickups[i]->GetID() == PickupIDs::CHARGE_UP)
			{
				// check if pickup isn't already active
				if(!m_hud.IsChargeUp())
				{
					// set hud notification to active
					m_hud.SetChargeUp();
				}

				m_playerWeapon.chargeAmt *= 0.3f; // half weapon charge time
				m_chargeUpTimer = PICKUP_TIMER; // reset timer
				m_chargeUpSound->Play(); // play sound
			}

			// check ID of pickup
			if(m_pickups[i]->GetID() == PickupIDs::DOUBLE_DAMAGE)
			{
				// check if pickup isn't already active
				if(!m_hud.IsDoubleDMG())
				{
					// set hud notification to active 
					m_hud.SetDoubleDMG();
				}

				m_playerWeapon.damage *= 2; // double damage
				m_chargeUpTimer = PICKUP_TIMER; // reset timer
				m_doubleDMGSound->Play(); // play sound

			}

			// check ID of pickup
			if(m_pickups[i]->GetID() == PickupIDs::RAPID_FIRE)
			{
				// check if pickup isn't already active
				if(!m_hud.IsRapidFire())
				{
					// set hud activation active and play pickup sound
					m_hud.SetRapidFire();
				}
			}
			m_rapidFireSound->Play(); // play sound
			m_rapidFireTimer = PICKUP_TIMER; // reset timer
			
			// delete pickup and remove from list
			delete m_pickups[i];
			m_pickups.erase(m_pickups.begin() + i);
		}
	}

	////////////////////////////////////////////////////////////////
	// update skybox
	m_skyBox->SetPosition(m_camera->GetPosition());
	m_skyBox->Update();

	////////////////////////////////////////////////////////////////
	// update hud
	m_hud.Update(deltaTime);

	// set weapon charge as fraction of 1.0f
	float chargebar = 0.0f;
	chargebar = m_playerWeapon.timer / m_playerWeapon.chargeAmt;
	
	// set charge bar value
	m_hud.ChargeWeapon(chargebar);

	////////////////////////////////////////////////////////////////
	// update audio engine
	// if audio is set to retry
	if(m_retryAudio)
	{
		// reset retry audio bool to false
		m_retryAudio = false;
		// if engine can be reset
		// restart any looped sounds
		if(m_audio->Reset())
		{
			// restart any looped sounds here
		}
	}
	else if(!m_audio->Update()) // unable to update audio engine
	{
		if(m_audio->IsCriticalError()) // if there's an error with audio device retry
		{
			m_retryAudio = true; // flag audio engine to reset next on next update
		}
	}

	////////////////////////////////////////////////////////////////
	// timers
	if(m_keyDelay > KEY_PRESS_DELAY)
	{	
		// key delay is greater than timer amount
		// reset delay timer and set 
		// key pressed boolean to false
		// to allow the keys to be pressed next frame
		m_keyDelay = 0;
		m_keyPressed = false;
	}
	m_keyDelay += deltaTime; // increase key delay timer
	
	// if player isn't allowed to shoot yet
	if(!m_playerWeapon.canShoot)
	{
		// check if weapon cooldown has finished
		if(m_playerWeapon.timer < 0.0f)
		{
			// allow shooing when timer hits charge time
			m_playerWeapon.canShoot = true;
		}

		m_playerWeapon.timer -= deltaTime; // weapon timer
	}

	// check if charge up is active
	if(m_hud.IsChargeUp())
	{
		// reduce timer
		m_chargeUpTimer -= deltaTime;

		// check if timer
		if(m_chargeUpTimer < 0.0f)
		{
			m_playerWeapon.chargeAmt = WEAPON_CHARGE_TIME;
			m_hud.SetChargeUp(); // toggle hud notification
			m_chargeUpTimer = PICKUP_TIMER; // reset timer
		}
	}

	// check if double damage is active
	if(m_hud.IsDoubleDMG())
	{
		// reduce timer
		m_doubleDMGTimer -= deltaTime;

		// check if timer
		if(m_doubleDMGTimer < 0.0f)
		{
			m_playerWeapon.damage = WEAPON_DAMAGE;
			m_hud.SetDoubleDMG(); // toggle hud notification
			m_doubleDMGTimer = PICKUP_TIMER; // reset timer
		}
	}

	// check if rapid fire is active
	if(m_hud.IsRapidFire())
	{
		// reduce timer
		m_rapidFireTimer -= deltaTime;
		
		// reset weapon charge
		m_playerWeapon.chargeAmt = WEAPON_CHARGE_TIME;

		// check if timer
		if(m_rapidFireTimer < 0.0f)
		{
			m_hud.SetRapidFire(); // toggle hud notification
			m_rapidFireTimer = PICKUP_TIMER; // reset timer
		}
	}

	////////////////////////////////////////////////////////////////
	// win condition
	// calculate distance from player to finish position
	float distanceToFinish;
	distanceToFinish = Vector3::Distance(m_player->GetPosition(), Utility::GridToWorldSpace((unsigned int)m_finishPosition.x, (unsigned int)m_finishPosition.y));

	// if distance is less than 1 unit change level
	if(distanceToFinish < 1.0f)
	{
		ChangeLevel();
	}
}

void
Game::Render()
{
	m_graphics->SetViewMatrix(m_camera->GetViewMatrix()); // set view matrix
	m_graphics->SetProjectionMatrix(m_camera->GetProjectionMatrix()); // set projection matrix

	// prepare graphics render target and clear backbuffer
	m_graphics->BeginScene();

	// render skybox
	m_skyBox->Render(m_graphics);

	// render maze
	// loop through object list and call render function
	for(unsigned int i = 0; i < m_mazeObjects.size(); i++)
	{
		m_mazeObjects[i]->Render(m_graphics); // render object
	}

	// render pickups
	for(unsigned int i = 0; i < m_pickups.size(); i++)
	{
		m_pickups[i]->Render(m_graphics);
		m_pickups[i]->RenderShadow(m_graphics);
	}
	
	// render NPCs
	for(unsigned int i = 0; i < m_NPCList.size(); i++)
	{
		m_NPCList[i]->RenderShadow(m_graphics);
		m_NPCList[i]->Render(m_graphics);
	}	

	// begin sprite batch rendering
	m_spriteBatch->Begin();

	// render overlay hud
	m_hud.Render(m_spriteBatch);

	// end sprite batch rendering
	m_spriteBatch->End();

	// display backbuffer on screen
	m_graphics->PresentBackBuffer();
}

void
Game::ReleaseAll()
{
	// release all pointer related release functions
	if(m_weaponSound) { m_weaponSound->Stop(); }
	if(m_skyBoxMesh) { m_skyBoxMesh->Release(); }
	if(m_shadowMesh) { m_shadowMesh->Release(); }
	if(m_NPCMesh) { m_NPCMesh->Release(); }
	if(m_pathMesh) { m_pathMesh->Release(); }
	if(m_cubeMesh) { m_cubeMesh->Release(); }
	if(m_skyBoxTexture) { m_skyBoxTexture->Release(); }
	if(m_shadowTexture) { m_shadowTexture->Release(); }
	if(m_rapidFireCrateTexture) { m_rapidFireCrateTexture->Release(); }
	if(m_doubleDMGCrateTexture) { m_doubleDMGCrateTexture->Release(); }
	if(m_chargeUpCrateTexture) { m_chargeUpCrateTexture->Release(); }
	if(m_NPCTexture) { m_NPCTexture->Release(); }
	if(m_pathTexture) { m_pathTexture->Release(); }
	if(m_exitTexture) { m_exitTexture->Release(); }
	if(m_entryTexture) { m_entryTexture->Release(); }
	if(m_wallTexture) { m_wallTexture->Release(); }
	if(m_spriteShader) { m_spriteShader->Release(); }
	if(m_skyBoxShader) { m_skyBoxShader->Release(); }
	if(m_simpleShader)	{ m_simpleShader->Release(); }
	if(m_graphics) { m_graphics->ReleaseAll();	}

	// reset unique ptrs
	for(unsigned int i = 0; i < m_NPCList.size(); i++)
	{
		m_NPCList[i]->Release(); // npc has a sound effect instance to realease
	}
	m_weaponSound.reset();
	m_audio.reset();
}

void
Game::DeleteAll()
{
	// delete rapid fire pickup
	if(m_rapidFireSound)
	{
		delete m_rapidFireSound;
		m_rapidFireSound = nullptr;
	}

	// delete double damage pickup
	if(m_doubleDMGSound)
	{
		delete m_doubleDMGSound;
		m_doubleDMGSound = nullptr;
	}

	// delete charge up pickup
	if(m_chargeUpSound)
	{
		delete m_chargeUpSound;
		m_chargeUpSound = nullptr;
	}

	// delete npc dead sound
	if(m_NPCDead)
	{
		delete m_NPCDead;
		m_NPCDead = nullptr;
	}

	// delete npc hit sound
	if(m_NPCHit)
	{
		delete m_NPCHit;
		m_NPCHit = nullptr;
	}

	// delete npc movement sound
	if(m_NPCMovement)
	{
		delete m_NPCMovement;
		m_NPCMovement = nullptr;
	}

	// delete energy gun sound
	if(m_energygunshot)
	{
		delete m_energygunshot;
		m_energygunshot = nullptr;
	}

	// delete skybox
	if(m_skyBox)
	{
		delete m_skyBox;
		m_skyBox = nullptr;
	}

	// delete shadow object
	if(m_shadow)
	{
		delete m_shadow;
		m_shadow = nullptr;
	}

	// delete level objects
	DeleteItems();
	DeleteNPCs();
	DeleteMaze();
		
	// delete skybox mesh
	if(m_skyBoxMesh)
	{
		delete m_skyBoxMesh;
		m_skyBoxMesh = nullptr;
	}

	// delete shadow mesh	
	if(m_shadowMesh)
	{
		delete m_shadowMesh;
		m_shadowMesh = nullptr;
	}

	// delete NPC mesh
	if(m_NPCMesh)
	{
		delete m_NPCMesh;
		m_NPCMesh = nullptr;
	}

	// delete Path mesh
	if(m_pathMesh)
	{
		delete m_pathMesh;
		m_pathMesh = nullptr;
	}

	// delete wall mesh
	if(m_cubeMesh)
	{
		delete m_cubeMesh;
		m_cubeMesh = nullptr;
	}
	
	// delete skybox material
	if(m_skyBoxMaterial)
	{
		delete m_skyBoxMaterial;
		m_skyBoxMaterial = nullptr;
	}

	// delete shadow Material
	if(m_shadowMaterial)
	{
		delete m_shadowMaterial;
		m_shadowMaterial = nullptr;
	}

	// delete rapid fire crate material
	if(m_rapidFireCrateMaterial)
	{
		delete m_rapidFireCrateMaterial;
		m_rapidFireCrateMaterial = nullptr;
	}

	// delete double dmg crate material
	if(m_doubleDMGCrateMaterial)
	{
		delete m_doubleDMGCrateMaterial;
		m_doubleDMGCrateMaterial = nullptr;
	}

	// delete charge up crate material
	if(m_chargeUpCrateMaterial)
	{
		delete m_chargeUpCrateMaterial;
		m_chargeUpCrateMaterial = nullptr;
	}

	// delete NPC material
	if(m_NPCMaterial)
	{
		delete m_NPCMaterial;
		m_NPCMaterial = nullptr;
	}

	// delete Path material
	if(m_pathMaterial)
	{
		delete m_pathMaterial;
		m_pathMaterial = nullptr;
	}

	// delete exit door material
	if(m_exitMaterial)
	{
		delete m_exitMaterial;
		m_exitMaterial = nullptr;
	}

	// delete entry door material
	if(m_entryMaterial)
	{
		delete m_entryMaterial;
		m_entryMaterial = nullptr;
	}

	// delete wall material
	if(m_wallMaterial)
	{
		delete m_wallMaterial;
		m_wallMaterial = nullptr;
	}
	
	// delete skybox Texture
	if(m_skyBoxTexture)
	{
		delete m_skyBoxTexture;
		m_skyBoxTexture = nullptr;
	}
	
	// delete shadow texture
	if(m_shadowTexture)
	{
		delete m_shadowTexture;
		m_shadowTexture = nullptr;
	}

	// delete rapid fire crate texture
	if(m_rapidFireCrateTexture)
	{
		delete m_rapidFireCrateTexture;
		m_rapidFireCrateTexture = nullptr;
	}

	// delete double dmg crate texture
	if(m_doubleDMGCrateTexture)
	{
		delete m_doubleDMGCrateTexture;
		m_doubleDMGCrateTexture = nullptr;
	}

	// delete chargeup crate texture
	if(m_chargeUpCrateTexture)
	{
		delete m_chargeUpCrateTexture;
		m_chargeUpCrateTexture = nullptr;
	}
	
	// delete NPC texture
	if(m_NPCTexture)
	{
		delete m_NPCTexture;
		m_NPCTexture = nullptr;
	}

	// delete Path Texture
	if(m_pathTexture)
	{
		delete m_pathTexture;
		m_pathTexture = nullptr;
	}
	
	// delete exit door texture
	if(m_exitTexture)
	{
		delete m_exitTexture;
		m_exitTexture = nullptr;
	}

	// delete entry door texture
	if(m_entryTexture)
	{
		delete m_entryTexture;
		m_entryTexture = nullptr;
	}

	// delete wall texture
	if(m_wallTexture)
	{
		delete m_wallTexture;
		m_wallTexture = nullptr;
	}

	// delete sprite shader
	if(m_spriteShader)
	{
		delete m_spriteShader;
		m_spriteShader = nullptr;
	}

	// delete shader
	if(m_skyBoxShader)
	{
		delete m_skyBoxShader;
		m_skyBoxShader = nullptr;
	}

	// delete shader
	if(m_simpleShader)
	{
		delete m_simpleShader;
		m_simpleShader = nullptr;
	}

	// delete 2d sprite batch engine
	if(m_spriteBatch)
	{
		delete m_spriteBatch;
		m_spriteBatch = nullptr;
	}

	// delete camera object
	if(m_camera)
	{
		delete m_camera;
		m_camera = nullptr;
	}

	// delete player pointer
	if(m_player)
	{
		delete m_player;
		m_player = nullptr;
	}

	// delete level manager
	if(m_levelManager)
	{
		delete m_levelManager;
		m_levelManager = nullptr;
	}

	// delete input object
	if(m_input)
	{
		delete m_input;
		m_input = nullptr;
	}

	// clear graphics object pointer
	if(m_graphics)
	{
		m_graphics = nullptr;
	}

	CoUninitialize();
}

void 
Game::NewGame()
{
	// initialise level manager
	if(!m_levelManager->Init())
	{
		char buffer[100]; // string buffer
		int d = m_levelManager->GetCurrentLevel(); // get current level number
		sprintf_s(buffer, " Error Loading Level %d in NewGame line 995 \n", d); // concatenate new string
		Error::FileLog(buffer); // log error to file

		MessageBox(m_graphics->GetHwnd(), L"Error loading level, see Logs/Error.txt", L"Error!", MB_OK); // display loading level error message
		PostQuitMessage(0); // quit game
	}

	DeleteMaze(); // delete maze wall and path objects
	DeleteNPCs(); // delete npcs
	DeleteItems(); // delete Items
	
	SpawnMaze(); // set maze wall and path objects
	SpawnNPCs(); // set enemy npc positions and waypoints
	SpawnItems(); // set item positions	// initialiase player
	
	m_player->Init();

	// set player spawn position
	m_player->SetGridPosX((int)m_startPosition.x);
	m_player->SetGridPosZ((int)m_startPosition.y);

	// initialise camera
	m_camera->Init(m_player->GetPosition() + HEAD_OFFSET, m_graphics->GetWidth(), m_graphics->GetHeight(), 0.1f, 100.0f);
	m_camera->LookAt(m_player->GetFacing());  // set view direction

	// set timer frequency
	m_timerFreq = (float)m_timer.GetFrequency();

	// seed the randoms with the current time
	srand((int)time(NULL));

	// set key delay
	m_keyDelay = KEY_PRESS_DELAY;

	// set weapon data
	m_playerWeapon.damage = WEAPON_DAMAGE;
	m_playerWeapon.chargeAmt = WEAPON_CHARGE_TIME;
	m_playerWeapon.timer = 0.0f;
	m_playerWeapon.range = WEAPON_RANGE;

	// reset pickup timers
	m_chargeUpTimer = PICKUP_TIMER;
	m_doubleDMGTimer = PICKUP_TIMER;
	m_rapidFireTimer = PICKUP_TIMER;

	ResetMouse(); // reset mouse pos to middle of game window
}

void
Game::LoadAssets()
{
	// load shaders
	m_simpleShader = new Shader();
	m_skyBoxShader = new Shader();
	m_spriteShader = new Shader();

	m_simpleShader->LoadShader(m_graphics, "Shaders\\SimpleShader.fx",
							   inputElementDesc,
							   ARRAYSIZE(inputElementDesc));

	m_skyBoxShader->LoadShader(m_graphics, 
							   "Shaders\\SkyShader.fx",
							   inputElementDesc,
							   ARRAYSIZE(inputElementDesc));


	m_spriteShader->LoadShader(m_graphics, "Shaders\\SimpleSprite.fx",
							   inputElementDesc,
							   ARRAYSIZE(inputElementDesc));

	// create new texture memory
	m_wallTexture	= new Texture();
	m_entryTexture	= new Texture();
	m_exitTexture	= new Texture();
	m_pathTexture	= new Texture();	
	m_NPCTexture	= new Texture();
	m_chargeUpCrateTexture	= new Texture();
	m_doubleDMGCrateTexture = new Texture();
	m_rapidFireCrateTexture = new Texture();
	m_shadowTexture = new Texture();
	m_skyBoxTexture = new Texture();

	// load textures
	m_wallTexture->LoadTexture(m_graphics, "Assets\\Textures\\wood.png");
	m_entryTexture->LoadTexture(m_graphics, "Assets\\Textures\\woodenentrance.png");
	m_exitTexture->LoadTexture(m_graphics, "Assets\\Textures\\woodenexit.png");
	m_pathTexture->LoadTexture(m_graphics, "Assets\\Textures\\path.png");
	m_NPCTexture->LoadTexture(m_graphics, "Assets\\Textures\\npc.png");
	m_chargeUpCrateTexture->LoadTexture(m_graphics, "Assets\\Textures\\woodencratechargeup.png");
	m_doubleDMGCrateTexture->LoadTexture(m_graphics, "Assets\\Textures\\woodencratedbldmg.png");
	m_rapidFireCrateTexture->LoadTexture(m_graphics, "Assets\\Textures\\woodencraterapidfire.png");
	m_shadowTexture->LoadTexture(m_graphics, "Assets\\Textures\\blobshadow.png");
	m_skyBoxTexture->LoadDDS(m_graphics, "Assets\\Textures\\nightskybox.dds");

	// create new material memory
	m_wallMaterial = new Material();
	m_entryMaterial = new Material();
	m_exitMaterial = new Material();
	m_pathMaterial = new Material();
	m_NPCMaterial = new Material();
	m_chargeUpCrateMaterial = new Material();
	m_doubleDMGCrateMaterial = new Material();
	m_rapidFireCrateMaterial = new Material();
	m_shadowMaterial = new Material();
	m_skyBoxMaterial = new Material();
	
	// load materials 
	m_wallMaterial->Init(m_wallTexture, m_simpleShader);
	m_entryMaterial->Init(m_entryTexture, m_simpleShader);
	m_exitMaterial->Init(m_exitTexture, m_simpleShader);
	m_pathMaterial->Init(m_pathTexture, m_simpleShader);
	m_NPCMaterial->Init(m_NPCTexture, m_spriteShader);
	m_chargeUpCrateMaterial->Init(m_chargeUpCrateTexture, m_simpleShader);
	m_doubleDMGCrateMaterial->Init(m_doubleDMGCrateTexture, m_simpleShader);
	m_rapidFireCrateMaterial->Init(m_rapidFireCrateTexture, m_simpleShader);
	m_shadowMaterial->Init(m_shadowTexture, m_simpleShader);
	m_skyBoxMaterial->Init(m_skyBoxTexture, m_skyBoxShader);

	// create memory for mesh data
	m_cubeMesh = new CubeMesh();
	m_pathMesh = new PathMesh();
	m_NPCMesh = new QuadMesh();
	m_shadowMesh = new ShadowMesh();
	m_skyBoxMesh = new SkyBoxMesh();

	// initialise mesh data
	m_cubeMesh->Init(m_graphics);
	m_pathMesh->Init(m_graphics);
	m_NPCMesh->Init(m_graphics);
	m_shadowMesh->Init(m_graphics);
	m_skyBoxMesh->Init(m_graphics);

	// setup shadow object
	m_shadow = new Shadow();
	m_shadow->Init(m_shadowMesh,
				   m_shadowMaterial);


	// setup skybox object
	m_skyBox = new SkyBox();
	m_skyBox->Init(m_skyBoxMesh,
				   m_skyBoxMaterial,
				   m_player->GetPosition());

	// load sounds
	m_energygunshot = new SoundEffect(m_audio.get(), L"Assets\\Sounds\\energygun.wav");
	m_NPCMovement = new SoundEffect(m_audio.get(), L"Assets\\Sounds\\robotmovement.wav");
	m_NPCHit = new SoundEffect(m_audio.get(), L"Assets\\Sounds\\robotshot.wav");
	m_NPCDead  = new SoundEffect(m_audio.get(), L"Assets\\Sounds\\robotdead.wav");
	m_chargeUpSound = new SoundEffect(m_audio.get(), L"Assets\\Sounds\\chargeuppickupsound.wav");
	m_doubleDMGSound = new SoundEffect(m_audio.get(), L"Assets\\Sounds\\doubledmgpickupsound.wav");
	m_rapidFireSound = new SoundEffect(m_audio.get(), L"Assets\\Sounds\\rapidfirepickupsound.wav");

	// setup hud overlay
	m_hud.Init(m_graphics, m_spriteBatch);
}

void Game::ChangeLevel()
{
	// clear containers
	m_levelManager->DeleteLevel();
	DeleteItems();
	DeleteNPCs();
	DeleteMaze();

	// switch level
	if(!m_levelManager->SwitchLevel())
	{
		// if this is the last level
		if(m_levelManager->GetCurrentLevel() == MAX_LEVELS)
		{
			// show player wins screen
			MessageBox(m_graphics->GetHwnd(), L"You made it to the end of the game", L"You Win!", MB_OK);
			PostQuitMessage(0); // quit game
			return;
		}
		else // level could not be loaded
		{
			char buffer[100]; // string buffer
			int d = m_levelManager->GetCurrentLevel(); // get current level number
			sprintf_s(buffer, "Error Loading Level %d during level switch in Game.cpp line 1126 \n", d); // print concatenated string to buffer
			Error::FileLog(buffer); // log error to file

			MessageBox(m_graphics->GetHwnd(), L"Error loading level, see Logs/Error.txt", L"Error!", MB_OK); // show error message
			PostQuitMessage(0); // quit game
		}
	}

	// spawn maze
	SpawnMaze();

	// spawn NPC
	SpawnNPCs();

	// spawn items
	SpawnItems();

	// initialiase player
	m_player->Init();

	// set player spawn position
	m_player->SetGridPosX((int)m_startPosition.x);
	m_player->SetGridPosZ((int)m_startPosition.y);

	// initialise camera
	m_camera->Init(m_player->GetPosition() + HEAD_OFFSET, m_graphics->GetWidth(), m_graphics->GetHeight(), 0.1f, 100.0f);
	m_camera->LookAt(m_player->GetFacing());  // set view direction

	// reset power ups
	if(m_hud.IsChargeUp()) // if charge up is active
	{
		m_hud.SetChargeUp(); // toggle notification
	}

	if(m_hud.IsDoubleDMG()) // if double damage is active
	{
		m_hud.SetDoubleDMG(); // toggle notification
	}

	if(m_hud.IsRapidFire()) // if rapid fire is active
	{
		m_hud.SetRapidFire(); // toggle notification
	}

	// set timer frequency
	m_timerFreq = (float)m_timer.GetFrequency();

	// seed the randoms with the current time
	srand((int)time(NULL));

	// reset weapon charge timer
	m_playerWeapon.timer = 0.0f;

	// set key delay
	m_keyDelay = KEY_PRESS_DELAY;

	ResetMouse(); // reset mouse pos to middle of game window
}

void
Game::ResetGame()
{
	// reset all game variables to play a new game
	m_input->ClearKeysPressed();

	// clear object containers
	DeleteItems();
	DeleteNPCs();
	DeleteMaze();

	// spawn game objects
	SpawnMaze();
	SpawnNPCs();
	SpawnItems();

	// initialiase player
	m_player->Init();

	// reset player variables
	m_player->SetGridPosX((int)m_startPosition.x);
	m_player->SetGridPosZ((int)m_startPosition.y);
	m_player->SetFacing(Vector3(0.0f, 0.0f, -1.0f));
	
	// reset camera variables
	m_camera->Init(m_player->GetPosition() + HEAD_OFFSET, m_graphics->GetWidth(), m_graphics->GetHeight(), 0.1f, 100.0f);
	m_camera->LookAt(m_player->GetFacing());  // set view direction

	// reset power ups
	if(m_hud.IsChargeUp()) // if charge up is active
	{
		m_hud.SetChargeUp(); // toggle notification
	}

	if(m_hud.IsDoubleDMG()) // if double damage is active
	{
		m_hud.SetDoubleDMG(); // toggle notification
	}

	if(m_hud.IsRapidFire()) // if rapid fire is active
	{
		m_hud.SetRapidFire(); // toggle notification
	}

	// set timer frequency
	m_timerFreq = (float)m_timer.GetFrequency();

	// set weapon data
	m_playerWeapon.damage = WEAPON_DAMAGE;
	m_playerWeapon.chargeAmt = WEAPON_CHARGE_TIME;
	m_playerWeapon.timer = 0.0f;
	m_playerWeapon.range = WEAPON_RANGE;

	// reseed randomness
	srand((int)time);

	// reset key delay
	m_keyDelay = KEY_PRESS_DELAY;
	m_keyPressed = false;
}

void 
Game::EndGame()
{
	// lose condition
	MessageBox(m_graphics->GetHwnd(), L"Unlucky, you were caught by the robot! Try again.", L"GAME OVER", MB_OK);
	NewGame();
}

void
Game::SpawnMaze()
{
	// add a cube to the renderable list
	// for each wall needed in the maze
	for(unsigned int z = 0; z < MAZE_DEPTH; z++)
	{
		for(unsigned int x = 0; x < MAZE_WIDTH; x++)
		{
			// for every wall value
			if(m_levelManager->GetLevel()->GetMap(x, z) == MapValues::WALL)
			{	
				// initialise cube object
				m_wallBlock = new MazeBlock();
				m_wallBlock->Init(m_cubeMesh,
								  m_wallMaterial,
								  Utility::GridToWorldSpace(x, z));

				// update wall block matrix with new position
				m_wallBlock->Update();

				// add cube to list of objects to render
				m_mazeObjects.push_back(m_wallBlock);
			}

			if(m_levelManager->GetLevel()->GetMap(x, z) == MapValues::PATH)

			{
				// setup Path object
				m_path = new Path();
				m_path->Init(m_pathMesh, 
							 m_pathMaterial,
							 Utility::GridToWorldSpace(x, z));

				// update path position
				m_path->Update();

				// add path to list of 
				m_mazeObjects.push_back(m_path);
			}

			if(m_levelManager->GetLevel()->GetMap(x, z) == MapValues::ENTRANCE)
			{
				// set up door object
				m_entryBlock = new MazeBlock();
				m_entryBlock->Init(m_cubeMesh,
								   m_entryMaterial,
								  Utility::GridToWorldSpace(x, z));

				// update door position
				m_entryBlock->Update();

				// add door block to object list
				m_mazeObjects.push_back(m_entryBlock);
			}

			if(m_levelManager->GetLevel()->GetMap(x, z) == MapValues::EXIT)
			{
				// set up door object
				m_exitBlock = new MazeBlock();
				m_exitBlock->Init(m_cubeMesh,
								  m_exitMaterial,
								  Utility::GridToWorldSpace(x, z));

				// update door position
				m_exitBlock->Update();

				// add door block to object list
				m_mazeObjects.push_back(m_exitBlock);
			}
		}
	}
}

void
Game::SpawnNPCs()
{
	// initialise NPC
	for(unsigned int z = 0; z < MAZE_DEPTH; z++)
	{
		for(unsigned int x = 0; x < MAZE_WIDTH; x++)
		{
			// for every NPC value
			if(m_levelManager->GetLevel()->GetNPCMap(x, z) == MapValues::NPC)
			{	
				// initialise NPC object
				m_NPC = new NPC();
				m_NPC->Init(m_NPCMesh,
							m_NPCMaterial,
							Utility::GridToWorldSpace(x,z));
				m_NPC->SetTarget(m_player);
				m_NPC->SetShadow(m_shadow);
				m_NPC->SetRandomDirection();
				m_NPC->SetSpeed(m_levelManager->GetLevel()->GetNPCData().speed);
				m_NPC->SetHP(m_levelManager->GetLevel()->GetNPCData().hp);
				m_NPC->SetMovementSFX(m_NPCMovement);
				m_NPC->SetHitSFX(m_NPCHit);
				m_NPC->SetDeadSFX(m_NPCDead);

				// add NPC to list of objects to render
				m_NPCList.push_back(m_NPC);
			}
		}
	}
}

void
Game::SpawnItems()
{
	// initialise items
	for(unsigned int z = 0; z < MAZE_DEPTH; z++)
	{
		for(unsigned int x = 0; x < MAZE_WIDTH; x++)
		{
			// set start position
			if(m_levelManager->GetLevel()->GetItemMap(x, z) == MapValues::START)
			{
				// initialise start position
				m_startPosition.x = (float)x;
				m_startPosition.y = (float)z;
			}

			// set finish position
			if(m_levelManager->GetLevel()->GetItemMap(x, z) == MapValues::FINISH)
			{
				// initialise level finish position
				m_finishPosition.x = (float)x;
				m_finishPosition.y = (float)z;
			}

			// set pickups
			if(m_levelManager->GetLevel()->GetItemMap(x, z) ==
				MapValues::CHARGE_UP) // if map location is chargeup 
			{
				// initialise weapon charge pick up
				m_chargeUpPickup = new Pickup();
				m_chargeUpPickup->Init(m_cubeMesh,
										m_chargeUpCrateMaterial,
										Utility::GridToWorldSpace(x, z));
				m_chargeUpPickup->SetShadow(m_shadow);
				
				// update position in world
				m_chargeUpPickup->Update();

				// set ID for collision detection 
				m_chargeUpPickup->SetID(PickupIDs::CHARGE_UP);

				// add pickup to pickups container
				m_pickups.push_back(m_chargeUpPickup);
			}

			if(m_levelManager->GetLevel()->GetItemMap(x, z) ==
				MapValues::DOUBLE_DAMAGE) // if map location is double dmg
			{
				// initialise double damage pickup
				m_doubleDMGPickup = new Pickup();
				m_doubleDMGPickup->Init(m_cubeMesh,
										m_doubleDMGCrateMaterial,
										Utility::GridToWorldSpace(x, z));
				m_doubleDMGPickup->SetShadow(m_shadow);

				// update position in world
				m_doubleDMGPickup->Update();

				// set ID for collision detection 
				m_doubleDMGPickup->SetID(PickupIDs::DOUBLE_DAMAGE);

				// add pickup up to pickups container
				m_pickups.push_back(m_doubleDMGPickup);
			}

			if(m_levelManager->GetLevel()->GetItemMap(x, z) ==
			   MapValues::RAPID_FIRE) // if map location is rapid fire
			{
				// initialise double damage pickup
				m_rapidFirePickup = new Pickup();
				m_rapidFirePickup->Init(m_cubeMesh,
										m_rapidFireCrateMaterial,
										Utility::GridToWorldSpace(x, z));
				m_rapidFirePickup->SetShadow(m_shadow);

				// update position in world
				m_rapidFirePickup->Update();

				// set ID for collision detection 
				m_rapidFirePickup->SetID(PickupIDs::RAPID_FIRE);

				// add pickup up to pickups container
				m_pickups.push_back(m_rapidFirePickup);
			}
		}
	}
}

void
Game::DeleteMaze()
{
	// clear current maze object list
	if(!m_mazeObjects.empty())
	{
		// delete all renderables from the list
		for(unsigned int i = 0; i < m_mazeObjects.size(); i++)
		{
			if(m_mazeObjects.max_size() > 0)
			{
				delete m_mazeObjects[i];
			}
		}

		// null pointers
		m_wallBlock = nullptr;
		m_entryBlock = nullptr;
		m_exitBlock = nullptr;
		m_path		= nullptr;

		// delete renderables list
		m_mazeObjects.clear();
	}
}

void 
Game::DeleteNPCs()
{
	// clear current enemy list
	if(!m_NPCList.empty())
	{
		// delete all NPC from the list
		for(unsigned int i = 0; i < m_NPCList.size(); i++)
		{
			delete m_NPCList[i];
		}

		m_NPC = nullptr; // null pointer

		// delete NPC list
		m_NPCList.clear();
	}
}

void Game::DeleteItems()
{
	if(!m_pickups.empty())
	{
		// delete all objects in container
		for(unsigned int i = 0; i < m_pickups.size(); i++)
		{
			delete m_pickups[i];
		}

		// null pointers
		m_chargeUpPickup = nullptr;
		m_doubleDMGPickup = nullptr;
		m_rapidFirePickup = nullptr;

		// clear container
		m_pickups.clear();
	}
}

void
Game::FireWeapon()
{
	m_hud.MuzzleFlash(); // render muzzle flash

	// create sound instance and play effect
	m_weaponSound = m_energygunshot->CreateInstance(SoundEffectInstance_ReverbUseFilters);
	m_weaponSound->Play();

	m_playerWeapon.canShoot = false; // player cannot shoot until weapon is charged
	
	// if rapid fire is active 
	// set charge time to rapid timer
	if(m_hud.IsRapidFire())
	{
		m_playerWeapon.timer = RAPID_FIRE_TIME;
	}
	else
	{
		m_playerWeapon.timer = m_playerWeapon.chargeAmt; // reset weapon charge
	}

	// loop through npc list
	for(unsigned int i = 0; i < m_NPCList.size(); i++)
	{
		if(m_NPCList[i]->GetState() != NPC_DEAD)
		{
			// check if player is in ai's viewable arc
			Vector3 line; // a line vector pointing from npc to player
			line = m_NPCList[i]->GetPosition() - m_player->GetPosition();

			float dot; // dot procuct
			dot = m_player->GetFacing().Dot(line);

			float playerDistance; // distance between player and npc
			playerDistance = line.Length();
			line.Normalize(); // normalise line vector

			if(dot > 0.6f && playerDistance < m_playerWeapon.range)
			{
				bool hidden = false;
				// loop through maze object list
				for(unsigned int j = 0; j < m_mazeObjects.size(); j++)
				{
					// if maze object is a wall its y position will be 0
					if(m_mazeObjects[j]->GetPosition().y == 0)
					{
						// get distance from object to NPC
						float objectDistance = 0; 
						objectDistance = (m_NPCList[i]->GetPosition() - m_mazeObjects[j]->GetPosition()).Length();

						// if distance to object is less than player
						if(objectDistance < playerDistance)
						{
							// check if the object is in between NPC and player
							if(Raycast(m_NPCList[i]->GetPosition(),
								m_player->GetPosition(),
								*m_mazeObjects[j]))
							{
								// the object is in between so NPC is hidden
								hidden = true;
								break; // no need to check any more objects
							}
						}
					}
				}
				// if npc isn't hidden behind an object it must have been 
				// hit by shot
				if(!hidden)
				{
					m_NPCList[i]->SetAIState(NPC_SHOT); // set ai state to shot
					m_NPCList[i]->SetAnimState(NPC_ANIM_SHOT); // set anim state to shot
					m_NPCList[i]->SetHP(m_NPCList[i]->GetHP() - m_playerWeapon.damage); // set hp after weapon dmg effect
				}
			}
		}
	}
}

void
Game::ResetMouse()
{
	// reset mouse cursor position to 
	// middle of game window
	POINT p;
	p.x = (long)m_graphics->GetWidth() / 2;
	p.y = (long)m_graphics->GetHeight() / 2;
	ClientToScreen(m_graphics->GetHwnd(), &p);
	SetCursorPos(p.x, p.y);
}

bool 
Game::Raycast(const Vector3& start, const Vector3& end, GameObject object)
{
	float maxLow = 0.0f; // max low point in ray
	float maxHigh = 1.0f; // max high point in ray

	float dimensionLow; // low point calculated from vectors
	float dimensionHigh; // high point calculated from vectors

	AABB aabbBox = object.GetHitBox(); // object aabb hitbox

	// check if rays are parallel
	Vector3 direction = end - start;
	if(direction.x == 0)
	{
		if(start.x < aabbBox.GetMin().x ||
		   start.x > aabbBox.GetMax().x)
		{
			return false;
		}
	}
	
	if(direction.z == 0)
	{
		if(start.z < aabbBox.GetMin().z ||
		   start.z > aabbBox.GetMax().z)
		{
			return false;
		}
	}

		// intersection point along x dimension
	dimensionLow = (aabbBox.GetMin().x - start.x) / direction.x;
	dimensionHigh = (aabbBox.GetMax().x - start.x) / direction.x;

	// check if low is less than high
	if(dimensionHigh < dimensionLow)
		std::swap(dimensionHigh, dimensionLow);

	// if intersections are out of bounds they must be false
	if(dimensionHigh < maxLow) return false;
	if(dimensionLow > maxHigh) return false;

	// Add the clip from this dimension to the previous results 
	maxLow = max(dimensionLow, maxLow);
	maxHigh = min(dimensionHigh, maxHigh);

	if(maxLow > maxHigh) return false; // max low point must be outside max high

	// intersection point along z dimension
	dimensionLow = (aabbBox.GetMin().z - start.z) / direction.z;
	dimensionHigh = (aabbBox.GetMax().z - start.z) / direction.z;

	// check if low is less than high
	if(dimensionHigh < dimensionLow)
		std::swap(dimensionHigh, dimensionLow);

	// if intersections are out of bounds they must be false
	if(dimensionHigh < maxLow) return false;
	if(dimensionLow > maxHigh)	return false;

	// Add the clip from this dimension to the previous results 
	maxLow = max(dimensionLow, maxLow);
	maxHigh = min(dimensionHigh, maxHigh);

	if(maxLow > maxHigh) return false; // max low point must be outside max high

	return true;
}

Vector2
Game::GetMidScreenPos()
{
	// get middle point of game window
	POINT p;
	p.x = (long)m_graphics->GetWidth() / 2;
	p.y = (long)m_graphics->GetHeight() / 2;
	ClientToScreen(m_graphics->GetHwnd(), &p);

	// set point to vector2 and return value
	Vector2 middle;
	middle.x = (float)p.x;
	middle.y = (float)p.y;
	return middle;
}

bool
Game::IsGridSpaceFree(unsigned int x, unsigned int z)
{
	// check if the grid reference returns PATH value from map
	if(m_levelManager->GetLevel()->GetMap(x, z) == MapValues::PATH)
	{
		// return true
		return true;
	}
	else
	{
		// return false
		return false;
	}
}

LRESULT 
Game::MessageHandler(HWND hWindow, UINT msg, WPARAM wParam, LPARAM lParam)
{
	// handle msg values in switch statement
	switch(msg)
	{
		case WM_DESTROY:
		PostQuitMessage(0); // post quit window
		return 0;
		case WM_KEYDOWN: case WM_SYSKEYDOWN:
		m_input->SetKeyDown(wParam); // set keyboard key down
		return 0;
		case WM_KEYUP: case WM_SYSKEYUP:
		m_input->SetKeyUp(wParam); // set keyboard key up
		return 0;
		case WM_MOUSEMOVE:
		m_input->SetMouseIn(lParam); // set mouse position
		return 0;
	}
	// else return default
	return DefWindowProc(hWindow, msg, wParam, lParam);
}