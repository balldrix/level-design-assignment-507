#include "Shader.h"
#include "pch.h"
#include "Graphics.h"
#include "Error.h"

Shader::Shader()
{
}

Shader::~Shader()
{
}

void 
Shader::LoadShader(Graphics* graphics, std::string filename, D3D11_INPUT_ELEMENT_DESC inputElementDesc[], UINT numElements)
{
	HRESULT result; // used for error checking

	// compile shaders
	ID3DBlob* pVSBlob = nullptr;
	ID3DBlob* pPSBlob = nullptr;

	// convert string to wstring LPCWSTR
	std::wstring shaderFile = std::wstring(filename.begin(), filename.end());

	result = D3DCompileFromFile(shaderFile.c_str(), nullptr, nullptr, "VShader", "vs_4_0", 0, 0, &pVSBlob, nullptr);

	// error checking
	if(result != S_OK)
	{
		// create error message for file log
		std::string error = " Error Compiling VShader file " + filename + " in Shader.cpp Line 25;\n";

		// call Error namespace to log file
		Error::FileLog(error);

		// display windows message box
		MessageBox(graphics->GetHwnd(), L"Shader Error. See Logs/Error.txt", L"Error!", MB_OK);

		PostQuitMessage(0); // quit game
	}

	result = D3DCompileFromFile(shaderFile.c_str(), nullptr, nullptr, "PShader", "ps_4_0", 0, 0, &pPSBlob, nullptr);

	// error checking
	if(result != S_OK)
	{
		std::string error = " Error Compiling PShader file " + filename + " in Shader.cpp Line 35;\n";

		// call Error namespace to log file
		Error::FileLog(error);

		// display windows message box
		MessageBox(graphics->GetHwnd(), L"Shader Error. See Logs/Error.txt", L"Error!", MB_OK);
		
		PostQuitMessage(0); // quit game
	}

	// create vertex shader
	graphics->GetDevice()->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), nullptr, &m_vertexShader);
	// create pixel shader
	graphics->GetDevice()->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), nullptr, &m_pixelShader);

	// create input layout
	graphics->GetDevice()->CreateInputLayout(inputElementDesc, numElements, pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), &m_vertexLayout);
}

void
Shader::Release()
{
	// release all shader related pointers
	if(m_vertexLayout) { m_vertexLayout->Release(); }
	if(m_pixelShader) { m_pixelShader->Release(); }
	if(m_vertexShader) { m_vertexShader->Release(); }
}
