#include "Graphics.h"
#include "Constants.h"
#include "ConstantBuffers.h"
#include "Error.h"

Graphics::Graphics() :
m_hWnd(nullptr),
m_hInstance(nullptr),			
m_swapchain(nullptr),
m_D3DDevice(nullptr),
m_D3DDeviceContext(nullptr),
m_renderTargetView(nullptr),
m_backbuffer(nullptr),
m_depthBuffer(nullptr),
m_depthBufferTexture(nullptr),
m_blendState(nullptr),
m_constantBuffer(nullptr),
m_textureTranslation(nullptr),
m_linearSampler(nullptr),
m_fullScreen(false),
m_gameWidth(0.0f),
m_gameHeight(0.0f)
{
}

Graphics::~Graphics()
{
}

void
Graphics::Init(HWND hWnd, HINSTANCE hInstance)
{
	HRESULT result; // used for error checking
	m_hWnd = hWnd;
	m_hInstance = hInstance;

	// get screen dimensions from window handle
	RECT rc;
	GetClientRect(m_hWnd, &rc);
	UINT width = rc.right - rc.left;
	UINT height = rc.bottom - rc.top;

	// Initialise DirectX
	DXGI_SWAP_CHAIN_DESC scd = { 0 };
	scd.BufferCount = 1;
	scd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	scd.BufferDesc.Width = width;
	scd.BufferDesc.Height = height;
	scd.BufferDesc.RefreshRate.Numerator = 60;
	scd.BufferDesc.RefreshRate.Denominator = 1;
	scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	scd.OutputWindow = m_hWnd;
	scd.SampleDesc.Count = 1;
	scd.SampleDesc.Quality = 0;
	scd.Windowed = !m_fullScreen;
	result = D3D11CreateDeviceAndSwapChain(NULL,
								  D3D_DRIVER_TYPE_HARDWARE,
								  NULL,
								  NULL,
								  NULL,
								  NULL,
								  D3D11_SDK_VERSION,
								  &scd,
								  &m_swapchain,
								  &m_D3DDevice,
								  NULL,
								  &m_D3DDeviceContext);

	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog("Error Creating D3D11 Device in Graphics.cpp Line 55; \n");
		
		MessageBox(m_hWnd, L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message
	
		PostQuitMessage(0); // quit game
	}

	// Get backbuffer pointer from swapchain
	result = m_swapchain->GetBuffer(0, __uuidof(m_backbuffer), (void**)&m_backbuffer);
	
	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog("Error getting Back Buffer in Graphics.cpp Line 76; \n");

		MessageBox(m_hWnd, L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message box

		PostQuitMessage(0); // quit game
	}

	// create render target view
	result = m_D3DDevice->CreateRenderTargetView(m_backbuffer, nullptr, &m_renderTargetView);

	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog("Error Creating Render Target in Graphics Line 86; \n");

		MessageBox(m_hWnd, L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message box

		PostQuitMessage(0); // quit game
	}

	m_backbuffer->Release();

	// depth buffer descripton
	D3D11_TEXTURE2D_DESC t2d = {0};
	t2d.Width				= width;
	t2d.Height				= height;
	t2d.MipLevels			= 1;
	t2d.ArraySize			= 1;
	t2d.Format				= DXGI_FORMAT_D24_UNORM_S8_UINT;
	t2d.SampleDesc.Count	= 1;
	t2d.BindFlags			= D3D11_BIND_DEPTH_STENCIL;

	// create depth buffer
	result = m_D3DDevice->CreateTexture2D(&t2d, NULL, &m_depthBufferTexture);

	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog("Error Creating Texture2D in Graphics.cpp Line 108; \n");

		MessageBox(m_hWnd, L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message box

		PostQuitMessage(0); // quit game
	}

	D3D11_DEPTH_STENCIL_VIEW_DESC dsvd;
	ZeroMemory(&dsvd, sizeof(dsvd));
	dsvd.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;	

	// create depth stencil view
	result = m_D3DDevice->CreateDepthStencilView(m_depthBufferTexture, &dsvd, &m_depthBuffer);

	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog("Error Creating DepthStencil in Graphics.cpp Line 122; \n");

		MessageBox(m_hWnd, L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message box

		PostQuitMessage(0); // quit game
	}

	// set blending state description
	D3D11_BLEND_DESC bdesc = { 0 };
	bdesc.RenderTarget[0].BlendEnable = true;
	bdesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	bdesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	bdesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	bdesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	bdesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	bdesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	bdesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	bdesc.RenderTarget[0].RenderTargetWriteMask = 0x0f;
	result = m_D3DDevice->CreateBlendState(&bdesc, &m_blendState);

	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog("Error Creating Blend State in Graphics.cpp Line 142; \n");

		MessageBox(m_hWnd, L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message box

		PostQuitMessage(0); // quit game
	}

	// initialise viewport
	D3D11_VIEWPORT vp;
	vp.Width = (float)width;
	vp.Height = (float)height;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	m_D3DDeviceContext->RSSetViewports(1, &vp);

	// set linear sampler
	D3D11_SAMPLER_DESC samplerbd = {};
	samplerbd.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerbd.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerbd.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerbd.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerbd.ComparisonFunc = D3D11_COMPARISON_NEVER;
	samplerbd.MinLOD = 0;
	samplerbd.MaxLOD = D3D11_FLOAT32_MAX;
	result = m_D3DDevice->CreateSamplerState(&samplerbd, &m_linearSampler);

	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog("Error Creating Sampler State in Graphics.cpp Line 170; \n");

		MessageBox(m_hWnd, L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message box

		PostQuitMessage(0); // quit game
	}

	m_D3DDeviceContext->PSSetSamplers(0, 1, &m_linearSampler);

	// setup constant buffer for matrices
	D3D11_BUFFER_DESC cbd = { 0 };
	cbd.Usage = D3D11_USAGE_DEFAULT;
	cbd.ByteWidth = sizeof(ConstantBuffer);
	cbd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	result = m_D3DDevice->CreateBuffer(&cbd, nullptr, &m_constantBuffer);

	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog("Error Creating Constant Buffer in Graphics.cpp Line 186; \n");

		MessageBox(m_hWnd, L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message box

		PostQuitMessage(0); // quit game
	}

	// setup constant buffer for texture uv translation
	cbd.ByteWidth = sizeof(TextureTranslation);
	m_D3DDevice->CreateBuffer(&cbd, nullptr, &m_textureTranslation);

	// error checking
	if(result != S_OK)
	{
		// log error in txt file
		Error::FileLog("Error Creating Constant Buffer in Graphics.cpp Line 197; \n");

		MessageBox(m_hWnd, L"Graphics Init Error. See Logs/Error.txt", L"Error!", MB_OK); // message

		PostQuitMessage(0); // quit game
	}

	// set game dimensions
	m_gameWidth = (float)width;
	m_gameHeight = (float)height;
}

void Graphics::ReleaseAll()
{
	// release all com pointers
	if(m_linearSampler) { m_linearSampler->Release(); }
	if(m_textureTranslation) { m_textureTranslation->Release(); }
	if(m_constantBuffer) { m_constantBuffer->Release(); }
	
	if(m_blendState) { m_blendState->Release();  }

	if(m_depthBufferTexture) { m_depthBufferTexture->Release(); }
	if(m_depthBuffer) { m_depthBuffer->Release(); }

	if(m_renderTargetView) { m_renderTargetView->Release(); }
	if(m_D3DDeviceContext) { m_D3DDeviceContext->Release(); }
	if(m_D3DDevice) { m_D3DDevice->Release(); }
	if(m_swapchain) { m_swapchain->Release(); }
}

void Graphics::SetWorldMatrix(Matrix world)
{
	// create final matrix to pass to constant buffer
	Matrix FinalMatrix = world * m_viewMatrix * m_projectionMatrix;

	// create constant buffer for vertex shader
	ConstantBuffer cb;
	cb.m_finalMatrix = XMMatrixTranspose(FinalMatrix);
	m_D3DDeviceContext->UpdateSubresource(m_constantBuffer, 0, nullptr, &cb, 0, 0);
	m_D3DDeviceContext->VSSetConstantBuffers(0, 1, &m_constantBuffer);
}

void Graphics::SetViewMatrix(Matrix view)
{
	m_viewMatrix = view;
}

void Graphics::SetProjectionMatrix(Matrix projection)
{
	m_projectionMatrix = projection;
}

void Graphics::SetUVTransform(Matrix transform)
{
	// create constant buffer for vertex shader
	TextureTranslation tt;
	tt.m_textTransMatrix = XMMatrixTranspose(transform);
	m_D3DDeviceContext->UpdateSubresource(m_textureTranslation, 0, nullptr, &tt, 0, 0);
	m_D3DDeviceContext->VSSetConstantBuffers(1, 1, &m_textureTranslation);
}

void
Graphics::BeginScene()
{
	// bind render target view and depth stencil view in graphics pipeline
	m_D3DDeviceContext->OMSetRenderTargets(1, &m_renderTargetView, m_depthBuffer);

	// set blend state for alpha blending
	m_D3DDeviceContext->OMSetBlendState(m_blendState, NULL, 0xffffffff);

	// clear depth buffer
	m_D3DDeviceContext->ClearDepthStencilView(m_depthBuffer, D3D11_CLEAR_DEPTH, 1.0f, 0);

	// clear backbuffer
	m_D3DDeviceContext->ClearRenderTargetView(m_renderTargetView, GlobalConstants::BACK_COLOUR);

	// reset Rasterisation state to normal state
	m_D3DDeviceContext->RSSetState(0);

	// reset depth stencil state to normal state
	m_D3DDeviceContext->OMSetDepthStencilState(0, 0);
}

void
Graphics::PresentBackBuffer()
{
	// present backbuffer
	m_swapchain->Present(0, 0);
}