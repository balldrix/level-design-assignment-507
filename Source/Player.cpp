#include "Player.h"
#include "pch.h"
#include "Constants.h"
#include "Utility.h"

Player::Player() :
m_positionX(0),
m_positionZ(0),
m_facing(0.0f,0.0f,0.0f),
m_angle(0.0f)
{
}

Player::~Player()
{
}

void 
Player::Init()
{
	m_hitbox.SetAABB(PLAYER_HIT_BOX); // set hit box vertices
	m_facing = Vector3(0.0f, 0.0, -1.0f); // set facing to default direction
	m_angle = 0.0f; // reset angle to 0
	m_rotationalMatrix = XMMatrixIdentity(); // reset rotational matrix

	// set default axis
	m_defaultXAxis = Vector3(1.0f, 0.0f, 0.0f);
	m_defaultYAxis = Vector3(0.0f, 1.0f, 0.0f);
	m_defaultZAxis = Vector3(0.0f, 0.0f, -1.0f);
}

void 
Player::Update(float deltaTime)
{
	// rotate facing direction
	m_facing = Vector3::Transform(m_defaultZAxis, m_rotationalMatrix);
	m_facing.Normalize();

	// update player position grid reference
	SetPosition(Utility::GridToWorldSpace(m_positionX, m_positionZ));

	// set hit box vertices
	m_hitbox.SetAABB(PLAYER_HIT_BOX);

	// update hotbox position
	m_hitbox.OffSetAABB(m_position);
}

void Player::SetFacing(Vector3 facing)
{
	m_facing = facing;
}

void Player::SetGridPosX(unsigned int x)
{
	m_positionX = x;
}

void Player::SetGridPosZ(unsigned int z)
{
	m_positionZ = z;
}

void
Player::StepForward()
{
	// z decreases as default facing is -Z
	m_positionX += (int)m_facing.x; // increase x by facing x
	m_positionZ -= (int)m_facing.z; // decrease z by facing z
}

void
Player::StepBackward()
{
	// z increase as default facing is -Z
	m_positionX -= (int)m_facing.x;
	m_positionZ += (int)m_facing.z;
}

void
Player::TurnRight()
{
	// increase angle by 90 degreed and rotate
	m_angle += XMConvertToRadians(90);
	m_rotationalMatrix = XMMatrixRotationAxis(m_defaultYAxis, m_angle);
}

void
Player::TurnLeft()
{
	// decrease angle by 90 degrees and rotate
	m_angle -= XMConvertToRadians(90);
	m_rotationalMatrix = XMMatrixRotationY(m_angle);
}