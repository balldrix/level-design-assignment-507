// SkyShader.fx
// Christopher Ball 2016
// Skymap Shader for use with the 
// skybox skymap surrounding the player

// constant buffers
cbuffer ConstantBuffer : register(b0)
{
	matrix FinalMatrix; // final matrix = world * view * projection
}

// PS globals
TextureCube skyMap; // skycube texture
SamplerState samplerState; // sampler state

// vertex shader input struct
struct VShaderInput
{
	float4 position : POSITION;	// semantics set in the input layout element desc
	float2 texcoord : TEXCOORD;
};

// pixel shader skymap input
struct PSSkyMapShaderInput
{
	float4 position : SV_POSITION; // semantics set in the input layout element desc
	float3 texcoord : TEXCOORD;
};

// Vertex Shader
PSSkyMapShaderInput VShader(VShaderInput input)
{
	PSSkyMapShaderInput output; // pixel shader output struct

	// setting position to xyww so z is always 0.9999f to keep it as far away as the far plane as possible
	output.position = mul(input.position, FinalMatrix).xyww * float4(1.0f, 1.0f, 0.9999f, 1.0f);
	
	// skymap uses 3d dds texture to texture coord are the same as input position
	output.texcoord = input.position;
	return output;
}

// Pixel Shader
float4 PShader(PSSkyMapShaderInput input) : SV_TARGET
{
	// return skymap sample using texture coordinates
	return skyMap.Sample(samplerState, input.texcoord);
}