// SimpleShader.fx
// Christopher Ball 2016
// Simple Shader is used for most 
// game objects that require no lighting

// constant buffers
cbuffer ConstantBuffer : register(b0)
{
	matrix FinalMatrix; // final matrix = world * view * projection
}

// PS globals
Texture2D normalTexture; // normal texture
SamplerState samplerState; // sampler state

// vertex shader input struct
struct VShaderInput
{
	float4 position : POSITION;	// semantics set in the input layout element desc
	float2 texcoord : TEXCOORD;
};

// pixel shader input struct
struct PShaderInput
{
	float4 position : SV_POSITION; // semantics set in the input layout element desc
	float2 texcoord : TEXCOORD;
};

// Vertex Shader
PShaderInput VShader(VShaderInput input)
{
	PShaderInput output; // pixel shader output struct
	output.position = mul(input.position, FinalMatrix); // multiply vertex position by final matrix
	output.texcoord = input.texcoord; // set texture cordinate
	return output;
}

// Pixel Shader
float4 PShader(PShaderInput input) : SV_TARGET
{
	// return normal texture sample with texture cordinates
	return normalTexture.Sample(samplerState, input.texcoord);
}