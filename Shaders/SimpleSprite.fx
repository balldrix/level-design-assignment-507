// SimpleSprite.fx
// Christopher Ball 2016
// Simple Sprite shader is used for 
// sprites that require some change
// to the texture coordinates such 
// as animations

// constant buffers
cbuffer ConstantBuffer : register(b0)
{
	matrix FinalMatrix; // final matrix = world * view * projection
}

cbuffer TextureTranslation : register(b1)
{
	matrix TexTransMatrix; // matrix of to translate texture coordinate 
}

// PS globals
Texture2D normalTexture; // normal texture
SamplerState samplerState; // sampler state;

// vertex shader input struct
struct VShaderInput
{
	float4 position : POSITION;	// semantics set in the input layout element desc
	float2 texcoord : TEXCOORD;
};

// pixel shader input struct
struct PShaderInput
{
	float4 position : SV_POSITION; // semantics set in the input layout element desc
	float2 texcoord : TEXCOORD;
};

// Vertex Shader
PShaderInput VShader(VShaderInput input)
{
	PShaderInput output; // pixel shader output struct
	output.position = mul(input.position, FinalMatrix); // multiply position by final matrix
	float3 newtexcoord = float3(input.texcoord, 1.0); // create new texture coordinate 
	output.texcoord = mul(newtexcoord, TexTransMatrix); // multiply texture coordinate by texture translation matrix
	return output;
}

// Pixel Shader
float4 PShader(PShaderInput input) : SV_TARGET
{
	// return normal texture sample with texture cordinates
	return normalTexture.Sample(samplerState, input.texcoord);
}